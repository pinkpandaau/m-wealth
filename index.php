<?php //get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<div class="blog_wrapper">
  <div class="container">
    <div class="row">
      <div class="blog_inner col-xl-7 col-lg-9">
        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php endwhile; ?>
      </div>
      <div class="side_bar col-xl-3 offset-xl-1 col-lg-3">
        <h3 class="heading">Categories</h3>
        <div class="category_count">
          <?php $categories = get_categories();
          foreach($categories as $category) {
            echo '<div class="category "><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a> <span>'. $category->count .'</span></div>';
          } ?>
        </div>
        <div class="category_block">
          <?php $categories = get_categories();
          foreach($categories as $category) {
            echo '<div class="block"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';
          } ?>
        </div>
        <!--<div class="archive">
          <h3 class="heading">Archive</h3>
          <?php wp_get_archives('show_post_count=1'); ?>
        </div>-->
      </div>
      <div class="pagination col-lg-12">
        <?php echo js_pagination(); ?>
      </div>
    </div>
  </div>
</div>

<?php //the_posts_navigation(); ?>
