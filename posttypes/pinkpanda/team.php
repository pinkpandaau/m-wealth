<?php

// Register Post Type
function create_team_post_type() {
	$cpt_labels = array(
	    'name' => __('Team', 'taxonomy general name', 'pink-panda'),
	    'singular_name' => __('Team Member', 'pink-panda'),
	    'search_items' => __('Search Team', 'pink-panda'),
	    'all_items' => __('Team', 'pink-panda'),
	    'parent_item' => __('Parent Team Member', 'pink-panda'),
	    'edit_item' => __('Edit Team Member', 'pink-panda'),
	    'update_item' => __('Update Team Member', 'pink-panda'),
	    'add_new_item' => __('Add New Team Member', 'pink-panda'),
	    'not_found' => __('No Team Member found', 'pink-panda')
	);

    $custom_slug = 'team';

    $args = array(
        'labels' => $cpt_labels,
        'rewrite' => array('slug' => $custom_slug, 'with_front' => false),
        'singular_label' => __('Team Member', 'pink-panda'),
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-businessperson',
        'supports' => array('title', 'editor', 'thumbnail')
    );
    register_post_type('team', $args);
    flush_rewrite_rules();
    //register_taxonomy("team_category", array("team"), array("hierarchical" => true, "label" => __('Team Member Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
}
add_action( 'init', 'create_team_post_type' );