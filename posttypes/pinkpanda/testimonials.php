<?php

// Register Post Type
function create_testimonials_post_type() {
	$cpt_labels = array(
	    'name' => __('Testimonials', 'taxonomy general name', 'pink-panda'),
	    'singular_name' => __('Testimonial', 'pink-panda'),
	    'search_items' => __('Search Testimonials', 'pink-panda'),
	    'all_items' => __('Testimonials', 'pink-panda'),
	    'parent_item' => __('Parent Testimonial', 'pink-panda'),
	    'edit_item' => __('Edit Testimonial', 'pink-panda'),
	    'update_item' => __('Update Testimonial', 'pink-panda'),
	    'add_new_item' => __('Add New Testimonial', 'pink-panda'),
	    'not_found' => __('No Testimonial found', 'pink-panda')
	);

    $custom_slug = 'testimonials';

    $args = array(
        'labels' => $cpt_labels,
        'rewrite' => array('slug' => $custom_slug, 'with_front' => false),
        'singular_label' => __('Testimonial', 'pink-panda'),
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'hierarchical' => false,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-format-chat',
        'supports' => array('title', 'editor', 'thumbnail')
    );
    register_post_type('testimonial', $args);
    flush_rewrite_rules();
    //register_taxonomy("testimonial_category", array("testimonial"), array("hierarchical" => true, "label" => __('Testimonial Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
}
add_action( 'init', 'create_testimonials_post_type' );