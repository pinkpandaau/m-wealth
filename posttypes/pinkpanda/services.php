<?php

// Register Post Type
function create_services_post_type() {
	$cpt_labels = array(
	    'name' => __('Services', 'taxonomy general name', 'pink-panda'),
	    'singular_name' => __('Service', 'pink-panda'),
	    'search_items' => __('Search Services', 'pink-panda'),
	    'all_items' => __('Services', 'pink-panda'),
	    'parent_item' => __('Parent Service', 'pink-panda'),
	    'edit_item' => __('Edit Service', 'pink-panda'),
	    'update_item' => __('Update Service', 'pink-panda'),
	    'add_new_item' => __('Add New Service', 'pink-panda'),
	    'not_found' => __('No Service found', 'pink-panda')
	);

    $custom_slug = 'service';

    $args = array(
        'labels' => $cpt_labels,
        'rewrite' => array('slug' => $custom_slug, 'with_front' => true),
        'singular_label' => __('Service', 'pink-panda'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-hammer',
        'supports' => array('title', 'editor', 'thumbnail')
    );
    register_post_type('service', $args);
    flush_rewrite_rules();
    //register_taxonomy("service_category", array("service"), array("hierarchical" => true, "label" => __('Service Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
    //register_taxonomy("division_category", array("service"), array("hierarchical" => true, "label" => __('Division Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
}
add_action( 'init', 'create_services_post_type' );
