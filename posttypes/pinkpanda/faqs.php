<?php

// Register Post Type
function create_faqs_post_type() {
	$cpt_labels = array(
	    'name' => __('FAQs', 'taxonomy general name', 'pink-panda'),
	    'singular_name' => __('FAQ', 'pink-panda'),
	    'search_items' => __('Search FAQs', 'pink-panda'),
	    'all_items' => __('FAQs', 'pink-panda'),
	    'parent_item' => __('Parent FAQ', 'pink-panda'),
	    'edit_item' => __('Edit FAQ', 'pink-panda'),
	    'update_item' => __('Update FAQ', 'pink-panda'),
	    'add_new_item' => __('Add New FAQ', 'pink-panda'),
	    'not_found' => __('No FAQ found', 'pink-panda')
	);

    $custom_slug = 'faqs';

    $args = array(
        'labels' => $cpt_labels,
        'rewrite' => array('slug' => $custom_slug, 'with_front' => false),
        'singular_label' => __('FAQ', 'pink-panda'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-editor-help',
        'supports' => array('title', 'editor', 'thumbnail')
    );
    register_post_type('faq', $args);
    flush_rewrite_rules();
    //register_taxonomy("faq_category", array("faq"), array("hierarchical" => true, "label" => __('FAQ Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
}
add_action( 'init', 'create_faqs_post_type' );