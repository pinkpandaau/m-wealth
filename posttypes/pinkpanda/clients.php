<?php

// Register Post Type
function create_clients_post_type() {
	$cpt_labels = array(
	    'name' => __('Clients', 'taxonomy general name', 'pink-panda'),
	    'singular_name' => __('Client', 'pink-panda'),
	    'search_items' => __('Search Clients', 'pink-panda'),
	    'all_items' => __('Clients', 'pink-panda'),
	    'parent_item' => __('Parent Client', 'pink-panda'),
	    'edit_item' => __('Edit Client', 'pink-panda'),
	    'update_item' => __('Update Client', 'pink-panda'),
	    'add_new_item' => __('Add New Client', 'pink-panda'),
	    'not_found' => __('No Client found', 'pink-panda')
	);

    $custom_slug = 'clients';

    $args = array(
        'labels' => $cpt_labels,
        'rewrite' => array('slug' => $custom_slug, 'with_front' => false),
        'singular_label' => __('Client', 'pink-panda'),
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'hierarchical' => false,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-groups',
        'supports' => array('title', 'editor')
    );
    register_post_type('client', $args);
    flush_rewrite_rules();
    //register_taxonomy("client_category", array("client"), array("hierarchical" => true, "label" => __('Client Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
}
add_action( 'init', 'create_clients_post_type' );