<div class="page_not_found container">

    <div class="row">

    	<div class="col-lg-10 offset-lg-1">
    	
    	    <div class="alert alert-warning">
				<?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
			</div>
    	
    	</div><!-- end col-lg-10 offset-lg-1 -->
	   
	</div><!-- end container -->

</div><!-- end default_page_wrapper row -->
