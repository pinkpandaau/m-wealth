<?php
/**
 * Template Name: Book Consultation Page
 */
?>

<div class="book_consult_wrapper">
    <div class="container">
        <div class="row">
            <div class="col_content col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
    
                <div class="form_wrapper">
                
                    <?php 
                        $formID = get_field('form_id');
                        if ($formID) {
                            echo do_shortcode('[gravityform id="'.$formID.'" title="false" description="false" ajax="true"]');
                        }
                    ?>
                
                </div><!-- end form_wrapper -->
            
            </div><!-- end col-6 -->
        </div>
    </div>
</div>

<?php get_template_part('templates/flexible-content'); ?>