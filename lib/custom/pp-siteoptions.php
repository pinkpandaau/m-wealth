<?php 

if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'General Settings',
    'menu_title'  => 'Website Options',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false,
    'icon_url' => 'dashicons-admin-appearance', // Add this line and replace the second inverted commas with class of the icon you like
    'position' => 2
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Contact Information',
    'menu_title'  => 'Contact Details',
    'parent_slug' => 'theme-general-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Default Values',
    'menu_title'  => 'Default Values',
    'parent_slug' => 'theme-general-settings',
  ));


  acf_add_options_sub_page(array(
    'page_title'  => 'The Journey',
    'menu_title'  => 'The Journey',
    'parent_slug' => 'theme-general-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'theme-general-settings',
  ));

}

?>