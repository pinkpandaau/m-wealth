<?php

// Disable WordPress Admin Bar for all users but admins
//show_admin_bar(true);

// Site by Pink Panda in WP Dashboard
function pinkpanda_dashboard_attribution($footer) {
	echo 'Site by <a href="http://www.pinkpanda.com.au">Pink Panda Digital Solutions</a>';
}

add_filter('admin_footer_text', 'pinkpanda_dashboard_attribution');

// Remove WP version for non-admins
function pinkpanda_remove_wp_version() {
    if (!current_user_can('manage_options') ) {
        remove_filter( 'update_footer', 'core_update_footer' ); 
    }
}
add_action( 'admin_menu', 'pinkpanda_remove_wp_version' );

// Remove widgets from dasboard
function pinkpanda_dashboard_cleanup() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
} 
add_action('wp_dashboard_setup', 'pinkpanda_dashboard_cleanup' );



// Customize Login Screen
function pinkpanda_login_screen() {

    ?>

    <style type="text/css">

        body {
            position: relative;
        }

        body:before {
             background: url('<?php echo get_bloginfo('template_directory'); ?>/assets/images/admin/powered-by-pink-panda-logo.png');
             background-size: 100%;
             background-repeat: no-repeat;
             content: '';
             width: 160px;
             height: 100px;
             position: absolute;
             bottom: 0px;
             left: 0px;
             right: 0px;
             margin-left: auto;
             margin-right: auto;
        }
        @media (max-width: 400px) {
            body:before {
                display: none;
            }
        }

        input {
            box-shadow: none !important;
            border: 1px solid #e0e0e0 !important;
        }

        input[type="submit"] {
            /*font-family: "Lato" !important;*/
            background-color: rgb(246,0,100) !important;
            /*padding: 5px 15px !important;
            height: 35px !important;*/
            border-radius: 0px !important;
        }

        input[type="submit"]:hover {
            background-color: rgb(230, 45, 120) !important;
        }

        .wp-core-ui .button-primary {
            text-shadow: none !important;
        }

        .login h1 a {
            background: url('<?php echo get_bloginfo('template_directory'); ?>/assets/images/admin/logo.svg') no-repeat center !important;
            background-size: 80% !important;
            width: 100% !important;
            background-size: contain !important;
            /*height: 150px;&/
        }

        .login {
            /*background-image: url('<?php echo get_bloginfo('template_directory'); ?>/assets/images/admin/admin-texture.jpg');*/
            background-repeat: repeat;
            /*background-color: #f45;*/
        }

        @media (min-width: 2000px) {
            #login {
                padding: 6% 0 0;
            }

        }

    </style>

<?php }
add_action('login_head', 'pinkpanda_login_screen');

// Remove 'Forgot Password' link
function pinkpanda_remove_lost_password ( $text ) {
	 if ($text == 'Lost your password?'){$text = '';}
		return $text;
	 }
add_filter( 'gettext', 'pinkpanda_remove_lost_password' );



// Force login with email address only
remove_filter('authenticate', 'wp_authenticate_username_password', 20);
add_filter('authenticate', 'pinkpanda_force_email_login', 20, 3);
function pinkpanda_force_email_login($user, $email, $password){

    //Check for empty fields
    if(empty($email) || empty ($password)){        
        //create new error object and add errors to it.
        $error = new WP_Error();

        if(empty($email)){ //No email
            $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
        }
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
            $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
        }

        if(empty($password)){ //No password
            $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
        }

        return $error;
    }

    //Check if user exists in WordPress database
    $user = get_user_by('email', $email);

    //bad email
    if(!$user){
        $error = new WP_Error();
        $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
        return $error;
    }
    else{ //check password
        if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
            return $error;
        }else{
            return $user; //passed
        }
    }
}

// Change text at login screen to remove 'Username'
add_filter('gettext', 'pinkpanda_change_login_text', 20);
function pinkpanda_change_login_text($text){
    if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
        if('Username' == $text){
            return 'Email Address';
        }
				if('Username or Email'== $text){
            return 'Email Address';
        }
    }
    return $text;
}



function load_chatlio_widget() {
        echo '<script type="text/javascript">
    window._chatlio = window._chatlio||[];
    !function(){ var t=document.getElementById("chatlio-widget-embed");if(t&&window.ChatlioReact&&_chatlio.init)return void _chatlio.init(t,ChatlioReact);for(var e=function(t){return function(){_chatlio.push([t].concat(arguments)) }},i=["configure","identify","track","show","hide","isShown","isOnline", "page", "open", "showOrHide"],a=0;a<i.length;a++)_chatlio[i[a]]||(_chatlio[i[a]]=e(i[a]));var n=document.createElement("script"),c=document.getElementsByTagName("script")[0];n.id="chatlio-widget-embed",n.src="https://w.chatlio.com/w.chatlio-widget.js",n.async=!0,n.setAttribute("data-embed-version","2.3");
       n.setAttribute("data-widget-id","a5eeb271-5fe5-4c86-620d-2f78d56dd41f");
       c.parentNode.insertBefore(n,c);
    }();
</script>';
}
//add_action( 'admin_head', 'load_chatlio_widget' );

function dashboard_video_upload_images( $post, $callback_args ) {
    echo '<iframe width="100%" height="315" src="https://www.youtube.com/embed/CBTR8p1-h6s" frameborder="0" allowfullscreen></iframe>';
}

function dashboard_video_edit_pages( $post, $callback_args ) {
    echo '<iframe width="100%" height="315" src="https://www.youtube.com/embed/GZYtHsYA3NM" frameborder="0" allowfullscreen></iframe>';
}

function dashboard_video_add_a_user( $post, $callback_args ) {
    echo '<iframe width="100%" height="315" src="https://www.youtube.com/embed/4fZIovYLHWo" frameborder="0" allowfullscreen></iframe>';
}

// Setup dashboard Widget
function add_dashboard_widgets() {
    //wp_add_dashboard_widget('pinkpanda_add_post_metabox_1', 'How to upload images', 'dashboard_video_upload_images');
    //wp_add_dashboard_widget('pinkpanda_add_post_metabox_2', 'How to edit pages', 'dashboard_video_edit_pages');
    //wp_add_dashboard_widget('pinkpanda_add_post_metabox_3', 'How to add a user', 'dashboard_video_add_a_user');
}

// Register widget
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );


function removeDemoModeLink() { // Be sure to rename this function to something more unique
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
    }
}
add_action('init', 'removeDemoModeLink');

/** remove redux menu under the tools **/
add_action( 'admin_menu', 'remove_redux_menu',12 );
function remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}




//add_action( 'admin_footer', 'rv_custom_dashboard_widget' );
function rv_custom_dashboard_widget() {
    // Bail if not viewing the main dashboard page
    if ( get_current_screen()->base !== 'dashboard' ) {
        return;
    }
    ?>

    <div id="custom-id" class="welcome-panel">
        <div class="welcome-panel-content">
            <h2>Welcome to the <?php bloginfo(); ?> website</h2>
            <p class="about-description">Crafted by Pink Panda Digital Solutions</p>
            <div class="welcome-panel-column-container">
                <div class="welcome-panel-column">
                    <h3>Do you need help?</h3>
                    <p class="bottom_desc">If you need help updating a page or changing content, please feel free to send an email to <a href="support@pinkpanda.com.au">support@pinkpanda.com.au</a> and one of our team members will be happy to assist.</p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/admin/pink-panda-logo.png" class="pink_panda_logo" />
                </div>
                <div class="welcome-panel-column">
                    <h3>Next Steps</h3>
                    <ul>
                        <li><a href="/wp-admin/post-new.php?post_type=page" class="welcome-icon welcome-add-page">Add additional pages</a></li>
                        <li><a href="/wp-admin/post-new.php" class="welcome-icon welcome-write-blog">Add a blog post</a></li>
                        <li><a href="/" class="welcome-icon welcome-view-site">View your site</a></li>
                    </ul>
                </div>
                <div class="welcome-panel-column welcome-panel-last">
                    <h3>More Actions</h3>
                    <ul>
                        <li><div class="welcome-icon welcome-widgets-menus">Manage <a href="/wp-admin/widgets.php">widgets</a> or <a href="/wp-admin/nav-menus.php">menus</a></div></li>
                        <li><a href="https://codex.wordpress.org/First_Steps_With_WordPress" class="welcome-icon welcome-learn-more">Learn more about getting started</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($) {
            $('#welcome-panel').after($('#custom-id').show());
            $('#welcome-panel').hide();
        });
    </script>

<?php }


function pinkpanda_promotion_display() { ?>
    <iframe width="100%" height="250px" src="https://pinkpanda.com.au/wordpress-promotion/" target='_top'></iframe>
<?php }


function register_pinkpanda_promo_widget() {
    global $wp_meta_boxes;

    wp_add_dashboard_widget(
        'pinkpanda_promo_widget',
        'Promotion',
        'pinkpanda_promotion_display'
    );

    $dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

    $my_widget = array( 'pinkpanda_promo_widget' => $dashboard['pinkpanda_promo_widget'] );
    unset( $dashboard['pinkpanda_promo_widget'] );

    $sorted_dashboard = array_merge( $my_widget, $dashboard );
    $wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}
//add_action( 'wp_dashboard_setup', 'register_pinkpanda_promo_widget' );

function clean_phone($phone_number) {
    $return_phone = str_replace(array('(',')',' '),'',$phone_number);
    $return_phone = 'tel:'.$return_phone;
    return $return_phone;
}

function disable_rest_endpoints ( $endpoints ) {
if ( isset( $endpoints['/wp/v2/users'] ) ) {
  unset( $endpoints['/wp/v2/users'] );
}
if ( isset( $endpoints['/wp/v2/users/(?P[\d]+)'] ) ) {
  unset( $endpoints['/wp/v2/users/(?P[\d]+)'] );
}
return $endpoints;
}
add_filter( 'rest_endpoints', 'disable_rest_endpoints');

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
