<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'topbar_navigation' => __('Topbar Navigation', 'sage'),
    //'primary_left_menu' => __('Primary Left Menu', 'sage'),
    //'primary_right_menu' => __('Primary Right Menu', 'sage'),
    'footer_links_1' => __('Footer Links 1', 'sage'),
    'footer_links_2' => __('Footer Links 2', 'sage'),
    'mobile_menu' => __('Mobile Menu', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  /*register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);*/
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Register top bar widgets
 */
function pinkpanda_topbar_widgets_init() {

    // First topbar widget area
    register_sidebar( array(
        'name' => __( 'Topbar Widget Area 1', 'pink-panda' ),
        'id' => 'topbar-widget-area-1',
        'description' => __( 'The first footer widget area 1', 'pink-panda' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<span style="display: none">',
        'after_title' => '</span>',
    ) ); 

    register_sidebar( array(
        'name' => __( 'Topbar Widget Area 2', 'pink-panda' ),
        'id' => 'topbar-widget-area-2',
        'description' => __( 'The first footer widget area 2', 'pink-panda' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<span style="display: none">',
        'after_title' => '</span>',
    ) ); 
      
}
add_action( 'widgets_init',  __NAMESPACE__ . '\\pinkpanda_topbar_widgets_init' );

/**
 * Register footer widgets
 */
function pinkpanda_footer_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area 1', 'pink-panda' ),
        'id' => 'second-footer-widget-area-1',
        'description' => __( 'The second footer widget area 1', 'pink-panda' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area 2', 'pink-panda' ),
        'id' => 'second-footer-widget-area-2',
        'description' => __( 'The second footer widget area 2', 'pink-panda' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area 3', 'pink-panda' ),
        'id' => 'second-footer-widget-area-3',
        'description' => __( 'The second footer widget area 3', 'pink-panda' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area 4', 'pink-panda' ),
        'id' => 'second-footer-widget-area-4',
        'description' => __( 'The second footer widget area 4', 'pink-panda' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-title">',
        'after_title' => '</h3>',
    ) );
     
}
add_action( 'widgets_init',  __NAMESPACE__ . '\\pinkpanda_footer_widgets_init' );

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    array('is_singular', array('cabin', 'performer')),
    is_page_template(['template-custom.php', 'template-container.php']),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {

  //wp_enqueue_style('pinkpanda_fonts', Assets\asset_path('styles/fonts.css'), false, null);
  wp_enqueue_style('pinkpanda_main_css', Assets\asset_path('styles/main.css'), false, null);

  if (is_home() || is_front_page()) {
    wp_enqueue_style('home', Assets\asset_path('styles/home.css'), false, null);
  }

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('pinkpanda_main_js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


// Enqueue admin.css
function assets_admin() {
    wp_enqueue_style( 'custom_wp_admin_css', Assets\asset_path('styles/adminstyles.css'), false, null);
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\assets_admin', 100 );




if (!defined('GOOGLE_FONTS')) {
  //define('GOOGLE_FONTS', 'Raleway:300,400,500,600|Open Sans');
}

function load_google_fonts() {
      
  if( ! defined( 'GOOGLE_FONTS' ) ) return;

  $pipe_ascii = '%7c';
  $google_fonts = str_replace("|", $pipe_ascii, GOOGLE_FONTS);

  if (is_ssl()) {
    echo '<link href="https://fonts.googleapis.com/css?family=' . $google_fonts . '" rel="stylesheet" type="text/css" />'."\n";
  } else {
    echo '<link href="http://fonts.googleapis.com/css?family=' . $google_fonts . '" rel="stylesheet" type="text/css" />'."\n";
  }

}
add_action( 'wp_head', __NAMESPACE__ . '\\load_google_fonts' , 1);
