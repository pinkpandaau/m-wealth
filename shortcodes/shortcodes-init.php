<?php

// ! File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$shortcodes = [
  'pinkpanda/pp-button.php'
];

foreach ($shortcodes as $file) {
  $file = 'shortcodes/' . $file;
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating shortcode - ' . $file, 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);