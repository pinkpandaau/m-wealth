<?php use Roots\Sage\Titles; ?>



<?php if (is_page_template('template-contact.php')) { ?>

    <div class="top_rap">
        <div class="container">
            <div class="row">
                <div class="breadcrumb col-lg-4 offset-lg-1">
                    <div class="">
                        <?php if(function_exists('bcn_display')) {
                            bcn_display();
                        } ?>
                    </div>
                    <h2 class="heading"><?php echo get_breadcrumb_title(); ?></h2>
                </div>
                <div class="detail col-lg-7">
                    <div class="contact large-para col-lg-4">
                        <div >Call</div>
                        <a href="<?php echo clean_phone(get_field('phone', 'options')); ?>"><?php echo get_field('phone', 'options'); ?></a>
                    </div>
                    <div class="location large-para col-lg-8">
                        <div>Visit</div>
                        <a href="https://www.google.co.in/maps/place/<?php echo get_field('address', 'options'); ?>"><?php echo get_field('address', 'options'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } else if (is_page_template('template-book-consultation.php')) { ?>

    <div class="top_rap">
        <div class="container">
            <div class="row">
                <div class="breadcrumb text-center col-lg-12">
                    <div class="">
                        <?php if(function_exists('bcn_display')) {
                            bcn_display();
                        } ?>
                    </div>
                    <h2 class="heading"><?php echo get_breadcrumb_title(); ?></h2>
                </div>
            </div>
        </div>
    </div>
    
<?php } else { ?>

    <?php

    $postID = get_the_ID();
    if (is_home()) {
        $postID = get_option( 'page_for_posts' );
    }

    if (is_singular('service')) {
        $background_override = get_field('background_override');
        if ($background_override) {
            $image_data = get_image_data($background_override);
        } else {
            $image_data = get_image_data(get_field('default_service_image', 'option'));
        }
    } else {
        $image_data = get_image_data(get_post_thumbnail_id($postID));    
    }
    ?>

    <div class="breadcrumbs_wrapper">

        <div class="breadcrumbs <?php if(!$image_data){ echo 'no_thumb'; }?>">

            <?php if (!empty($image_data)) { ?>
                <img
                    src="<?php echo $image_data['url']; ?>"
                    srcset="<?php echo $image_data['srcset']; ?>"
                    sizes="100vw"
                    width="<?php echo $image_data['width']; ?>"
                    height="<?php echo $image_data['height']; ?>"
                    alt="<?php echo $image_data['alt']; ?>"
                    class="img_tag_bg"
                />
            <?php } ?>

            <div class="breadcrumb-inner">
                <div class="single-member-bread">
                    <div class="container">
                        <div class="row">
                            <div class="member_detail col-lg-10 offset-lg-1">
                                <div class="inner">
                                    <?php if(function_exists('bcn_display')) {
                                        bcn_display();
                                    } ?>
                                    <h1><?php echo get_breadcrumb_title(); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- breadcrumbs -->

    </div><!-- end breadcrumbs_wrapper -->

<?php } ?>