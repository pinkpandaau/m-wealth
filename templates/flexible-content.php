<?php if( have_rows('content_blocks') ): ?>

	<?php while( have_rows('content_blocks') ): the_row(); ?>

        <?php if( get_row_layout() == 'hero_slideshow'): ?>

			<?php get_template_part('templates/sections/hero-slideshow'); ?>

		<?php elseif( get_row_layout() == 'panel_grid'): ?>

			<?php get_template_part('templates/sections/panel-grid'); ?>

		<?php elseif( get_row_layout() == 'the_journey'): ?>

			<?php get_template_part('templates/sections/the-journey'); ?>

		<?php elseif( get_row_layout() == 'testimonials'): ?>

			<?php get_template_part('templates/sections/testimonials'); ?>

		<?php elseif( get_row_layout() == 'mini_carousel'): ?>

			<?php get_template_part('templates/sections/mini-carousel'); ?>

		<?php elseif( get_row_layout() == 'services'): ?>

			<?php get_template_part('templates/sections/services'); ?>

		<?php elseif( get_row_layout() == 'call_to_action'): ?>

			<?php get_template_part('templates/sections/call-to-action'); ?>

		<?php elseif( get_row_layout() == 'latest_news'): ?>

			<?php get_template_part('templates/sections/latest-news'); ?>

		<?php elseif( get_row_layout() == 'our_team'): ?>

			<?php get_template_part('templates/sections/our-team'); ?>

		<?php elseif( get_row_layout() == 'text_block'): ?>

			<?php get_template_part('templates/sections/text-block'); ?>

		<?php elseif( get_row_layout() == 'our_process'): ?>

			<?php get_template_part('templates/sections/our-process'); ?>

		<?php elseif( get_row_layout() == 'our_business'): ?>

			<?php get_template_part('templates/sections/our-business'); ?>

		<?php endif; ?>

	<?php endwhile; ?>
<?php endif; ?>