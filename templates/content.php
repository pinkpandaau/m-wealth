<div class="single_blog col-lg-12">
    <div class="row">
        <div class="col_image col-lg-6">
            <a href="<?php the_permalink(); ?>">

                <?php
                $image_data = get_image_data(get_post_thumbnail_id(), 'rect-sm');
                if (!empty($image_data)) { ?>
                    <img
                        src="<?php echo $image_data['url']; ?>"
                        srcset="<?php echo $image_data['srcset']; ?>"
                        sizes="100vw"
                        width="<?php echo $image_data['width']; ?>"
                        height="<?php echo $image_data['height']; ?>"
                        alt="<?php echo $image_data['alt']; ?>"
                    />
                <?php } else { ?> 
                    <span class="no-thumb"></span>
                <?php } ?>

            </a>
        </div>
        <div class="col_content col-lg-6">
            <?php $categories = get_the_category(); $count = count($categories);
                $output = '';
                if ( ! empty( $categories ) ) {?>
                <div class="cats_block">
                    <?php foreach( $categories as $category => $cat ) {
                        if (($category + 1) != $count) {
                            $output .= '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '" class="small-para mb-0 orange-light" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $cat->name ) ) . '">' . esc_html( $cat->name ) .', </a>';
                        } else {
                            $output .= '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '" class="small-para mb-0 orange-light" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $cat->name ) ) . '">' . esc_html( $cat->name ) .'</a>';
                        }
                    }
                    echo trim( $output ); ?>
                </div>
            <?php } ?>
            <a href="<?php the_permalink(); ?>"><h3 class="max-para"><?php the_title(); ?></h3></a>
            <div class="large-para"><?php the_excerpt(); ?></div>
            <div class="link-wrapper">
                <a href="<?php the_permalink(); ?>" class="link_extended small-para">
                    Read more <svg xmlns="http://www.w3.org/2000/svg" width="20.572" height="12.54" viewBox="0 0 20.572 12.54">
                    <g id="Group_2195" data-name="Group 2195" transform="translate(0 0.345)">
                        <line id="Line_1" data-name="Line 1" x1="19.882" transform="translate(0 5.925)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                        <path id="Path_1" data-name="Path 1" d="M24.331,17.836l5.643-5.925L24.331,5.986" transform="translate(-10.092 -5.986)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                    </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</div>
      