<header class="banner">

    <div class="container thin-container">

        <div class="row">

            <div class="col_topbar col-12">

                <nav class="topbar-navigation">
                    <?php
                        if (has_nav_menu('topbar_navigation')) :
                            wp_nav_menu(['theme_location' => 'topbar_navigation', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav', 'menu_id' => 'topbar-menu']);
                        endif;
                    ?><!--end #desktop-menu -->
                </nav>

            </div><!-- end col_topbar -->

        </div><!-- end row -->

        <div class="row main-header-row">

            <div class="col_logo col-lg-3 col-md-8 col-xs-8">

                <a class="site_logo" href="<?= esc_url(home_url('/')); ?>">
                    <img src='<?php echo get_template_directory_uri(); ?>/dist/images/logo/m-wealth-logo.svg' alt="M-Wealth Logo" id="header-logo" class="img-responsive">
                </a>

            </div><!-- end col-lg-3 -->

            <div class="col_menu_wrapper col-lg-9 d-none d-lg-block">
                <nav class="nav-primary">
                    <?php
                        if (has_nav_menu('primary_navigation')) :
                            wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav', 'menu_id' => 'desktop-menu']);
                        endif;
                    ?><!--end #desktop-menu -->
                </nav>
            </div>

            <div class="col_hamburger_wrapper d-lg-none">
                <a id="hamburger-icon" class="closed hamburger hamburger--collapse mobile-toggle" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </a>
            </div>

        </div><!-- end row -->

    </div><!-- end container -->

    <div id="mobile-navigation" class="mobile-nav">
        <div class="wrapped">
            <div class="inner">
                <div id="mobile-scrollable-wrapper">
                    <a href="<?= esc_url(home_url('/')); ?>">
                        <img src='<?php echo get_template_directory_uri(); ?>/dist/images/logo/m-wealth-logo-light.svg' alt="M-Wealth Footer Logo" id="footer-logo" class="img-responsive mx-auto d-block">
                    </a>
                    <?php if (has_nav_menu('mobile_menu')) :
                            wp_nav_menu(['theme_location' => 'mobile_menu', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav text-center mobile-menu', 'menu_id' => 'mobile-menu']);
                          endif; ?>


                    <a class="mobile_phone" href="<?php echo clean_phone(get_field('phone', 'options')); ?>"><?php echo get_field('phone', 'options'); ?></a>

                </div>


            </div>
        </div>
    </div>

    <div id="mobile-navigation-sticky" class="mobile-nav-sticky">
        <div class="wrapped">
            <div class="inner">
                <div id="mobile-scrollable-wrapper">
                    <?php if (has_nav_menu('mobile_menu')) :
                            wp_nav_menu(['theme_location' => 'mobile_menu', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav text-center mobile-menu', 'menu_id' => 'mobile-menu']);
                          endif; ?>
                    <a class="mobile_phone" href="<?php echo clean_phone(get_field('phone', 'options')); ?>"><?php echo get_field('phone', 'options'); ?></a>
                </div>

            </div>
        </div>
    </div>

    <div class="sticky-header" id="sticky-header">

        <div class="col_topbar">

            <div class="container">

                <div class="row">

                    <div class="col-12">

                        <nav class="topbar-navigation">
                            <?php
                                if (has_nav_menu('topbar_navigation')) :
                                    wp_nav_menu(['theme_location' => 'topbar_navigation', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav', 'menu_id' => 'topbar-menu']);
                                endif;
                            ?><!--end #desktop-menu -->
                        </nav>

                    </div><!-- end col_topbar -->

                </div><!-- end row -->

            </div>

        </div>

        <div class="container">

            <div class="row main-header-row">

                <div class="col_logo col-lg-3 col-md-8 col-xs-8">

                    <a class="site_logo" href="<?= esc_url(home_url('/')); ?>">
                        <img src='<?php echo get_template_directory_uri(); ?>/dist/images/logo/m-wealth-logo.svg' alt="M-Wealth Logo" id="header-logo" class="img-responsive">
                    </a>

                </div><!-- end col-lg-3 -->

                <div class="col_menu_wrapper col-lg-9 d-none d-lg-block">
                    <nav class="nav-primary">
                        <?php
                            if (has_nav_menu('primary_navigation')) :
                                wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav', 'menu_id' => 'desktop-menu']);
                            endif;
                        ?><!--end #desktop-menu -->
                    </nav>
                </div>

                <div class="col_hamburger_wrapper d-lg-none">
                    <a id="hamburger-icon" class="closed hamburger hamburger--collapse mobile-toggle-sticky" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </a>
                </div>

            </div><!-- end row -->

        </div>

    </div>

</header>

<script>
jQuery(".mobile-toggle").click(function() {

            var $this = jQuery(this),
            $mobileNav = $this.parents('.banner').find(".mobile-nav");
            $mobilemenuLi = $mobileNav.find(".mobile-menu > li");
            $mobilemenuNav = $mobileNav.find(".mobile-menu");

            jQuery('body').toggleClass('menu-open');
            $this.toggleClass('is-active');
            if(!$this.hasClass("closed")) {

                jQuery($mobilemenuLi.get().reverse()).each(function(i) {
                    TweenLite.to(jQuery(this), 0.2 * i, { ease: Power4.easeInOut, opacity:0});
                });

                TweenMax.to($mobileNav, 0.5, {ease: Power4.easeInOut, y:'-100%'});
                $mobileNav.removeClass("open");
                $this.addClass('closed');

            } else {

                setTimeout(function(){
                    $mobilemenuLi.each(function(i) {
                        TweenLite.to(jQuery(this), 0.3 * i, {ease: Power4.easeInOut, opacity:1});
                    });
                }, 300);

                $mobilemenuNav.css("maxHeight", $mobilemenuNav.height());
                TweenMax.to($mobileNav, 0.5, {ease: Power4.easeInOut, y:'0%'});
                $this.removeClass('closed');
                $mobileNav.addClass("open");

            }

        });
</script>

<script>
jQuery(".mobile-toggle-sticky").click(function() {

            var $this = jQuery(this),
            $mobileNav = $this.parents('.banner').find(".mobile-nav-sticky");
            $mobilemenuLi = $mobileNav.find(".mobile-menu > li");
            $mobilemenuNav = $mobileNav.find(".mobile-menu");

            jQuery('body').toggleClass('menu-open');
            $this.toggleClass('is-active');
            if(!$this.hasClass("closed")) {

                jQuery($mobilemenuLi.get().reverse()).each(function(i) {
                    TweenLite.to(jQuery(this), 0.2 * i, { ease: Power4.easeInOut, opacity:0});
                });

                TweenMax.to($mobileNav, 0.5, {ease: Power4.easeInOut, y:'-100%'});
                $mobileNav.removeClass("open");
                $this.addClass('closed');

            } else {

                setTimeout(function(){
                    $mobilemenuLi.each(function(i) {
                        TweenLite.to(jQuery(this), 0.3 * i, {ease: Power4.easeInOut, opacity:1});
                    });
                }, 300);

                $mobilemenuNav.css("maxHeight", $mobilemenuNav.height());
                TweenMax.to($mobileNav, 0.5, {ease: Power4.easeInOut, y:'0%'});
                $this.removeClass('closed');
                $mobileNav.addClass("open");

            }

        });
</script>

<script>
var triggerOffset = 600
    jQuery(window).scroll(function() {

        if (jQuery(this).scrollTop() > triggerOffset) {
            jQuery('#sticky-header').addClass('visible');

        } else {
            jQuery('#sticky-header').removeClass('visible');

        }
    });
</script>
