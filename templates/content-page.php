<div class="single_post">
    <div class="top_wrap col-lg-12">
        <div class="row">
            <div class="col-lg-2 offset-lg-1 blog_detail">
                <div class="author large-para">Written by <?php echo get_the_author_meta('display_name'); ?></div>
                <div class="date large-para"><?php echo the_date(); ?></div>
            </div>
            <div class="content col-xl-6 col-lg-8">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <div class="bottom_wrap">
        <div class="row">
            <div class="category_block col-lg-4 offset-lg-3">
                <?php $categories = get_categories();
                foreach($categories as $category) {
                    echo '<div class="block"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';
                } ?>
            </div>
            <div class="share col-lg-2">
                <div>Share</div>
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php the_permalink(); ?>" title="Share on LinkedIn">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/linkedin.svg" class="linkedin" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;m-wealth - <?php the_title(); ?>" title="Share on Facebook.">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/facebook.svg" class="facebook" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="mailto:?subject=m-wealth - <?php the_title(); ?>&amp;body=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Share by Email">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/email.svg" class="mail" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="recent col-xl-6 offset-xl-3 col-lg-10 offset-lg-1">
        <h2 class="title text-center orange-light subheading">You might also be interested in…</h2>
        <div class="posts">
            <div class="row">
                <?php
                $query = new WP_Query( array(
                    'post_type' => 'post',
                    'posts_per_page' => 2,
                    'order' => 'ASC',
                    'post__not_in' => array($post->ID)
                ) ); 

                if ( $query->have_posts() ) {   ?>

                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                        <div class="single_recent_post col-lg-6">
                            <div class="single">

                                <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' )[0]; ?>
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php echo esc_url($featured_image); ?>" alt="">
                                </a>
                                <div class="info_wrap">
                                    <div class="content">
                                        <h3 class="heading medium-heading"><?php the_title(); ?></h3>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="link-wrapper">
                                        <a href="<?php the_permalink(); ?>" class="link_extended small-para">
                                            Read more <svg xmlns="http://www.w3.org/2000/svg" width="20.572" height="12.54" viewBox="0 0 20.572 12.54">
                                                <g id="Group_2195" data-name="Group 2195" transform="translate(0 0.345)">
                                                    <line id="Line_1" data-name="Line 1" x1="19.882" transform="translate(0 5.925)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_1" data-name="Path 1" d="M24.331,17.836l5.643-5.925L24.331,5.986" transform="translate(-10.092 -5.986)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; wp_reset_postdata(); ?>

                <?php } ?>

            </div>
        </div>
    </div>
</div>