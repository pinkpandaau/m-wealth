<section class="mini_carousel_wrapper">

    <div class="row">

        <div class="col_content col-12">

            <?php if( have_rows('slideshow') ) { ?>

                <div class="mini_carousel_text">

                    <?php while ( have_rows('slideshow') ) : the_row(); ?>

                        <?php get_template_part('templates/sections/carousel/text-slide'); ?>

                    <?php endwhile; ?>

                </div><!-- end mini_carousel_text -->

            <?php } else { ?>


                <?php if( have_rows('slideshow_mini_carousel', 'option') ): ?>

                    <div class="mini_carousel_text">
                
                        <?php while ( have_rows('slideshow_mini_carousel', 'option') ) : the_row(); ?>
                    
                            <?php get_template_part('templates/sections/carousel/text-slide'); ?>
                    
                        <?php endwhile; ?>

                    </div>
                
                <?php endif; ?>

            <?php } ?>

            <?php if( have_rows('slideshow') ) { ?>

                <div class="mini_carousel">

                    <?php while ( have_rows('slideshow') ) : the_row(); ?>

                        <?php if(get_sub_field('background_image')) { ?> 

                            <?php get_template_part('templates/sections/carousel/image-slide'); ?>

                        <?php } ?>

                    <?php endwhile; ?>

                </div>

            <?php } else { ?>
                
                <?php if( have_rows('slideshow_mini_carousel', 'option') ): ?>

                    <div class="mini_carousel">

                        <?php while ( have_rows('slideshow_mini_carousel', 'option') ) : the_row(); ?>

                            <?php if(get_sub_field('background_image')) { ?> 

                                <?php get_template_part('templates/sections/carousel/image-slide'); ?>

                            <?php } ?>

                        <?php endwhile; ?>

                    </div>
                <?php endif; ?>

            <?php } ?>

        </div><!-- end col-lg-10 offset-lg-1 -->

    </div><!-- end row -->

</section><!-- end mini_carousel_wrapper -->


<script>
    jQuery(document).ready(function ($) {

        var $mini_carousel  = $('.mini_carousel');
        var $mini_carousel_text  = $('.mini_carousel_text');

        $mini_carousel.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            infinite: true,
            fade: true,
            arrows: false,
            dots: false,
            prevArrow:'<a href="" class="left"></a>',
            nextArrow:'<a href="" class="right"></a>',
            speed: 1000,
            lazyLoad: 'ondemand',
            adaptiveHeight: false,
            asNavFor: ".mini_carousel_text",
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        autoplay: false
                    }
                }
            ]
        });

        $mini_carousel_text.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            infinite: true,
            fade: false,
            arrows: false,
            dots: true,
            prevArrow:'<a href="" class="left"></a>',
            nextArrow:'<a href="" class="right"></a>',
            speed: 1000,
            lazyLoad: 'ondemand',
            adaptiveHeight: false,
            asNavFor: ".mini_carousel",
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        autoplay: false
                    }
                }
            ]
        });

    });
</script>
