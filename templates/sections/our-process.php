<div class="our_process_wrapper">
    <div class="container">
        <?php if( have_rows('our_processes') ):

            while( have_rows('our_processes') ) : the_row(); ?>

                <div class="single col-lg-12">
                    <div class="row">
                        <div class="left col-xl-3 offset-xl-1 col-lg-3 offset-lg-1">
                            <?php $heading = get_sub_field('heading'); ?>
                            <?php if ($heading) { ?>
                                <h3 class="subheading"><?php echo $heading; ?></h3>
                            <?php } ?>
                        </div>
                        <div class="right col-xl-6 col-lg-8">
                            <?php $body = get_sub_field('body'); ?>
                            <?php if ($body) { ?>
                                <p class="large-para"><?php echo $body; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php endwhile;

        endif; ?>
    </div>
</div>