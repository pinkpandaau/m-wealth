<?php
$bg_colour = get_sub_field('background_colour');
?>
<div class="text_and_image_wrapper" bg-colour="<?php echo $bg_colour; ?>">

    <div class="container">
    
        <div class="row">
        
            <div class="col_content col-lg-12">
            
                <?php $subheading = get_sub_field('subheading'); ?>
                <?php if ($subheading) { ?>
                    <h3 class="subheading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $subheading; ?></h3>
                <?php } ?>
    
                <?php $heading = get_sub_field('heading'); ?>
                <?php if ($heading) { ?>
                    <h2 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $heading; ?></h2>
                <?php } ?>
    
                <?php $body = get_sub_field('body'); ?>
                <?php if ($body) { ?>
                    <div class="body" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                        <?php echo $body; ?>
                    </div>
                <?php } ?>
    
                <?php 
                $small_link = get_sub_field('link');
                if( $small_link ): 
                    $small_link_url = $small_link['url'];
                    $small_link_title = $small_link['title'];
                    $small_link_target = $small_link['target'] ? $small_link['target'] : '_self';
                    ?>
                    <a data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800" class="link arrow" href="<?php echo esc_url( $small_link_url ); ?>" target="<?php echo esc_attr( $small_link_target ); ?>"><?php echo esc_html( $small_link_title ); ?></a>
                <?php endif; ?>
    
                <?php 
                $link = get_sub_field('button');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800" class="btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                <?php endif; ?>
            
            </div><!-- end col-6 -->

            <div class="col_image col-lg-6">
            
                <?php $the_image_id = get_sub_field('image');
                if($the_image_id) {
                    $image_url = wp_get_attachment_image_src( $the_image_id, 'rect-md' )[0];
                    $alt = get_post_meta( $the_image_id, '_wp_attachment_image_alt', true ); ?>
                    <img src="<?php echo $image_url; ?>" alt="<?php echo esc_attr($alt); ?>" class="img-responsive rounded" />
                <?php } ?>
            
            </div><!-- end col-6 -->
        
        </div><!-- end row -->
    
    </div><!-- end container -->

</div><!-- end text_and_image_wrapper -->