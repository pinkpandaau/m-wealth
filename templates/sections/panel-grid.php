<?php
$layout = get_sub_field('layout')
?>
<?php if ($layout == 'layout_1') { ?>
    <section class="panel_grid_wrapper">

        <div class="row">

            <?php if( have_rows('panels') ): ?>

                <?php $i = 1;?>

                <div class="col-xl-6 col-lg-12">

                    <div class="row h-100">

                        <?php while ( have_rows('panels') ) : the_row(); ?>

                                <?php $bg_colour = get_sub_field('background_colour'); ?>

                                <?php if($i == 3){
                                        $class="third-panel";
                                        }

                                        else if($i == 2){
                                            $class="second-panel";
                                        }

                                        else if($i == 1){
                                            $class="first-panel";
                                        }

                                ?>

                                <div class="panel col-lg-6 col-md-12 <?php echo $class;?>" bg-colour="<?php echo $bg_colour; ?>">

                                    <div class="inner">

                                        <?php $image_data = get_image_data(get_sub_field('icon'));
                                        //$image_data = get_image_data(get_post_thumbnail_id());
                                        if (!empty($image_data)) { ?>
                                            <img
                                                src="<?php echo $image_data['url']; ?>"
                                                srcset="<?php echo $image_data['srcset']; ?>"
                                                sizes="100vw"
                                                alt="<?php echo $image_data['alt']; ?>"
                                                class="img-responsive"
                                            />
                                        <?php } ?>


                                        <?php if (get_sub_field('heading')) { ?>
                                            <h3 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo get_sub_field('heading'); ?></h3>
                                        <?php } ?>

                                        <?php if (get_sub_field('body')) { ?>
                                            <div class="body large-para" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                                                <?php echo get_sub_field('body'); ?>
                                            </div>
                                        <?php } ?>


                                        <?php $image_data = get_image_data(get_sub_field('hover_image'));
                                        //$image_data = get_image_data(get_post_thumbnail_id());
                                        if (!empty($image_data)) { ?>
                                            <div class="bg_image">
                                            
                                                <img
                                                    src="<?php echo $image_data['url']; ?>"
                                                    srcset="<?php echo $image_data['srcset']; ?>"
                                                    sizes="100vw"
                                                    width="<?php echo $image_data['width']; ?>"
                                                    height="<?php echo $image_data['height']; ?>"
                                                    alt="<?php echo $image_data['alt']; ?>"
                                                    class="img_tag_bg"
                                                />
                                            
                                            </div><!-- end bg_image -->
                                            
                                        <?php } ?>


                                    </div>

                                </div><!-- end panel -->

                            <?php $i++;?>

                        <?php endwhile; ?>

                    </div>

                </div>

            <?php endif; ?>

            <div class="col-xl-6 col-lg-12">

                <div class="about-inner">

                    <?php $subheading = get_sub_field('subheading'); ?>
                    <?php if ($subheading) { ?>
                        <h3 class="subheading orange" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $subheading; ?></h3>
                    <?php } ?>

                    <?php if (get_sub_field('heading')) { ?>
                        <h2 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo get_sub_field('heading'); ?></h2>
                    <?php } ?>

                    <?php if (get_sub_field('body')) { ?>
                        <div class="body" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                            <?php echo get_sub_field('body'); ?>
                        </div>
                    <?php } ?>

                    <?php
                    $small_link = get_sub_field('link');
                    if( $small_link ):
                        $small_link_url = $small_link['url'];
                        $small_link_title = $small_link['title'];
                        $small_link_target = $small_link['target'] ? $small_link['target'] : '_self';
                        ?>
                        <a data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800" class="link arrow link-right-chevron" href="<?php echo esc_url( $small_link_url ); ?>" target="<?php echo esc_attr( $small_link_target ); ?>">
                            <?php echo esc_html( $small_link_title ); ?>
                            <svg xmlns="http://www.w3.org/2000/svg" width="3.562" height="5.96" viewBox="0 0 3.562 5.96">
                            <path id="Path_193" data-name="Path 193" d="M24.331,11.256l2.51-2.635-2.51-2.635" transform="translate(-23.969 -5.641)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </a>
                    <?php endif; ?>

                </div>

            </div>

        </div>

    </section><!-- end text_and_image_wrapper -->
<?php } else { ?>
    <section class="panel_grid_wrapper panel_grid_wrapper_block">

        <?php if( have_rows('panels') ): ?>

            <?php $i = 1;?>

            <div class="container">
                <div class="row  d-flex align-items-stretch">
                    <div class="col-lg-10 offset-lg-1">

                        <div class="row">
                            <?php while ( have_rows('panels') ) : the_row(); ?>

                                    <?php $bg_colour = get_sub_field('background_colour'); ?>

                                    <?php if($i == 3){
                                        $class="third-panel";
                                        }

                                        else if($i == 2){
                                            $class="second-panel";
                                        }

                                        else if($i == 1){
                                            $class="first-panel";
                                        }

                                    ?>

                                    <div class="panel panel_block col-lg-4 <?php echo $class;?>" bg-colour="<?php echo $bg_colour; ?>">

                                        <div class="inner">

                                            <?php $image_data = get_image_data(get_sub_field('icon'));
                                            //$image_data = get_image_data(get_post_thumbnail_id());
                                            if (!empty($image_data)) { ?>
                                                <img
                                                    src="<?php echo $image_data['url']; ?>"
                                                    srcset="<?php echo $image_data['srcset']; ?>"
                                                    sizes="100vw"
                                                    alt="<?php echo $image_data['alt']; ?>"
                                                    class="img-responsive"
                                                />
                                            <?php } ?>


                                            <?php if (get_sub_field('heading')) { ?>
                                                <h3 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo get_sub_field('heading'); ?></h3>
                                            <?php } ?>

                                            <?php if (get_sub_field('body')) { ?>
                                                <div class="body large-para" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                                                    <?php echo get_sub_field('body'); ?>
                                                </div>
                                            <?php } ?>


                                            <?php $image_data = get_image_data(get_sub_field('hover_image'));
                                            //$image_data = get_image_data(get_post_thumbnail_id());
                                            if (!empty($image_data)) { ?>
                                                <div class="bg_image">
                                                
                                                    <img
                                                        src="<?php echo $image_data['url']; ?>"
                                                        srcset="<?php echo $image_data['srcset']; ?>"
                                                        sizes="100vw"
                                                        width="<?php echo $image_data['width']; ?>"
                                                        height="<?php echo $image_data['height']; ?>"
                                                        alt="<?php echo $image_data['alt']; ?>"
                                                        class="img_tag_bg"
                                                    />
                                                
                                                </div><!-- end bg_image -->
                                                
                                            <?php } ?>

                                        </div>

                                    </div><!-- end panel -->

                                <?php $i++;?>

                            <?php endwhile; ?>
                        </div>

                    </div>
                </div>
            </div>

        <?php endif; ?>


    </section><!-- end text_and_image_wrapper -->
<?php } ?>