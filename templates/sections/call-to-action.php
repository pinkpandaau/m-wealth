<?php
$bg_colour = get_sub_field('background_colour');
?>
<section class="call_to_action_wrapper" bg-colour="<?php echo $bg_colour; ?>">

    <div class="container thin-container">

        <div class="row">

            <div class="col_content col-lg-6">

                <?php
                $subheading = get_sub_field('subheading');
                if (empty($subheading)) {
                    $subheading = get_field('cta_subheading', 'option');
                }
                ?>

                <?php if (!empty($subheading)) { ?>
                    <h3 class="subheading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $subheading; ?></h3>
                <?php } ?>

                <?php
                $heading = get_sub_field('heading');
                if (empty($heading)) {
                    $heading = get_field('cta_heading', 'option');
                }
                ?>

                <?php if (!empty($heading)) { ?>
                    <h2 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $heading; ?></h2>
                <?php } ?>

                <?php
                $body = get_sub_field('body');
                if (empty($body)) {
                    $body = get_field('cta_body', 'option');
                }
                ?>

                <?php if (!empty($body)) { ?>
                    <div class="body" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                        <?php echo $body; ?>
                    </div>
                <?php } ?>

            </div>

            <div class="col_content col_right col-lg-6 align-self-center">

                <div class="wrapper">

                    <?php
                    $small_link = get_sub_field('link');
                    if (empty($small_link)) {
                        $small_link = get_field('cta_link', 'option');
                    }

                    if ($small_link) {
                        $small_link_url = $small_link['url'];
                        $small_link_title = $small_link['title'];
                        $small_link_target = $small_link['target'] ? $small_link['target'] : '_self';
                        ?>
                        <a data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800" class="link arrow small-para" href="<?php echo esc_url($small_link_url); ?>" target="<?php echo esc_attr($small_link_target); ?>"><?php echo esc_html($small_link_title); ?></a>
                    <?php } ?>

                    <?php
                    $link = get_sub_field('button');
                    if (empty($link)) {
                        $link = get_field('cta_button', 'option');
                    }

                    if ($link) {
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <a data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800" class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    <?php } ?>

                </div>

            </div><!-- end col-6 -->


        </div><!-- end row -->

    </div><!-- end container -->

</section><!-- end text_and_image_wrapper -->
