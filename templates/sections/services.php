<?php $layoutType = get_sub_field('layout'); ?>
<section class="services_wrapper <?php if($layoutType === 'all'){echo 'servicesStacked';} ?>">

    <div class="container">

        <?php if (get_sub_field('heading')) {?>

            <div class="row">
    
                <div class="col_intro col-lg-12">
    
                    <h2 class="heading"><?php the_sub_field('heading');?></h2>
    
                </div><!-- end col_intro -->
    
            </div><!-- end row -->

        <?php } ?>

        <div class="row content_row">

            <?php if ($layoutType == 'tiles') { ?>

                <div class="col-lg-12">

                    <div class="row">

                        <?php $services = get_sub_field('services');

                        if( $services ): ?>

                            <?php foreach( $services as $post ):

                                // Setup this post for WP functions (variable must be named $post).
                                setup_postdata($post); ?>
                                <div class="col-lg-4 col-md-6 col-sm-12 col_service">

                                    <div class="inner">

                                        <div class="wrapper">

                                            <?php $the_image_id = get_field('icon'); ?>
                                            <?php if($the_image_id) {

                                                $image_url = wp_get_attachment_image_src( $the_image_id, 'full' )[0];
                                                $alt = get_post_meta( $the_image_id, '_wp_attachment_image_alt', true ); ?>
                                                <img class="dark-icon mx-auto d-block" src="<?php echo $image_url; ?>" alt="<?php echo esc_attr($alt); ?>" class="img-responsive" />

                                            <?php } ?>

                                            <h3><a class="max-para" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                
                                            <?php if(get_field('excerpt')) { ?> 
                                                <div class="excerpt large-para"><?php the_field('excerpt'); ?></div>
                                            <?php } ?>

                                        </div>

                                    </div><!-- end inner -->

                                    <?php //image_data = get_image_data(get_sub_field('field_name'));
                                    $image_data = get_image_data(get_post_thumbnail_id());
                                    if (!empty($image_data)) { ?>
                                        <div class="bg_image">
                                            
                                            <img
                                                src="<?php echo $image_data['url']; ?>"
                                                srcset="<?php echo $image_data['srcset']; ?>"
                                                sizes="100vw"
                                                width="<?php echo $image_data['width']; ?>"
                                                height="<?php echo $image_data['height']; ?>"
                                                alt="<?php echo $image_data['alt']; ?>"
                                                class="img_tag_bg"
                                            />
                                            <div class="overlay">
                                            </div>
                                        
                                        </div><!-- end bg_image -->
                                        
                                    <?php } ?>



                                </div>
                            <?php endforeach; ?>

                            <?php
                            // Reset the global post object so that the rest of the page works correctly.
                            wp_reset_postdata(); ?>
                        <?php endif; ?>

                    </div><!-- end row -->

                </div><!-- end col-lg-12 -->

            <?php } else if ($layoutType == 'all') { ?>

                <div class="col-lg-10 offset-lg-1">

                    <div class="row">
                
                        <?php
                        $query = new WP_Query( array(
                            'post_type' => 'service',
                            'posts_per_page' => -1,
                            'order' => 'ASC'
                        ) ); 

                        if ( $query->have_posts() ) {   ?>

                            <div class="grid col-lg-12">
                                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                    <div class="single_service">
                                        <div class="row">

                                            <div class="col_content col-xl-5 my-auto col-lg-5">

                                                <?php $the_image_id = get_field('icon'); ?>
                                                <?php if($the_image_id) {
                                                    $image_url = wp_get_attachment_image_src( $the_image_id, 'full' )[0];
                                                    $alt = get_post_meta( $the_image_id, '_wp_attachment_image_alt', true ); ?>
                                                    <img class="dark-icon" src="<?php echo $image_url; ?>" alt="<?php echo esc_attr($alt); ?>" class="img-responsive" />
                                                <?php } ?>

                                                <h4><?php the_title(); ?></h4>

                                                <?php if(get_field('excerpt')) { ?> 
                                                    <div class="large-para"><?php the_field('excerpt'); ?></div>
                                                <?php } ?>

                                                <a class="link arrow link-right-chevron" href="<?php the_permalink(); ?>">
                                                    LEARN MORE
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="3.562" height="5.96" viewBox="0 0 3.562 5.96">
                                                        <path id="Path_193" data-name="Path 193" d="M24.331,11.256l2.51-2.635-2.51-2.635" transform="translate(-23.969 -5.641)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                                                    </svg>
                                                </a>

                                            </div>
                                                    
                                            <div class="col_image col-xl-6 offset-xl-1 col-lg-7">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    $image_data = get_image_data(get_post_thumbnail_id(), 'full');
                                                    if (!empty($image_data)) { ?>
                                                        <img
                                                            src="<?php echo $image_data['url']; ?>"
                                                            srcset="<?php echo $image_data['srcset']; ?>"
                                                            sizes="100vw"
                                                            width="<?php echo $image_data['width']; ?>"
                                                            height="<?php echo $image_data['height']; ?>"
                                                            alt="<?php echo $image_data['alt']; ?>"
                                                        />
                                                    <?php } else { ?> 
                                                        <span class="no-thumb"></span>
                                                    <?php } ?>
                                                </a>

                                            </div>

                                        </div>
                                    </div>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </div>

                        <?php } ?>

                    </div><!-- end row -->

                </div><!-- end col-lg-10 offset-lg-1 -->

            <?php } else { ?>

                <div class="service_slide col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">

                    <?php 
                        $query = new WP_Query( array(
                            'post_type' => 'service',
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            'post__not_in' => array($post->ID)
                    ) ); ?>

                    <?php if ( $query->have_posts() ) : ?>
                        <div class="sevices_slider">
                            <?php while ( $query->have_posts() ) :
                                $query->the_post(); ?>
                                <div class="single_slide">

                                    <div class="col_service">   

                                        <div class="inner">

                                            <div class="wrapper">

                                                <?php $the_image_id = get_field('icon'); ?>
                                                <?php if($the_image_id) { 
                                                    $image_url = wp_get_attachment_image_src( $the_image_id, 'full' )[0];
                                                    $alt = get_post_meta( $the_image_id, '_wp_attachment_image_alt', true ); ?>
                                                    <img class="dark-icon mx-auto d-block" src="<?php echo $image_url; ?>" alt="<?php echo esc_attr($alt); ?>" class="img-responsive" />
                                                <?php } ?>

                                                <h3><a class="max-para" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                          

                                            </div>

                                        </div>

                                        <?php
                                        $image_data = get_image_data(get_post_thumbnail_id());
                                        if (!empty($image_data)) { ?>
                                            <div class="bg_image">
                                            
                                                <img
                                                    src="<?php echo $image_data['url']; ?>"
                                                    srcset="<?php echo $image_data['srcset']; ?>"
                                                    sizes="100vw"
                                                    width="<?php echo $image_data['width']; ?>"
                                                    height="<?php echo $image_data['height']; ?>"
                                                    alt="<?php echo $image_data['alt']; ?>"
                                                    class="img_tag_bg"
                                                />
                                                <div class="overlay">
                                                </div>

                                            </div><!-- end bg_image -->
                                            
                                        <?php } ?>

                                    </div>
                                        
                                </div>
                            <?php endwhile; wp_reset_postdata(); ?>
                        </div>
                        <div class="slider_actions">
                            <div class="custom_arrows sevice_arrow_left"></div>
                            <div class="custom_arrows sevice_arrow_right"></div>
                        </div>
                    <?php endif; ?>

                </div><!-- end col-lg-8 offset-lg-2 -->
                
            <?php } ?>

        </div>

        <?php if (get_sub_field('link')) { ?>

            <div class="row">
    
                <div class="col_link col-lg-10 offset-lg-1">
    
                    <?php
                    $link = get_sub_field('link');
                    if( $link ):
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <a class="link small-para blue link-right-chevron" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                            <?php echo esc_html( $link_title ); ?>
                            <svg xmlns="http://www.w3.org/2000/svg" width="3.562" height="5.96" viewBox="0 0 3.562 5.96">
                              <path id="Path_193" data-name="Path 193" d="M24.331,11.256l2.51-2.635-2.51-2.635" transform="translate(-23.969 -5.641)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </a>
                    <?php endif; ?>
    
                </div><!-- end col_link col-lg-10 offset-lg-1 -->
    
            </div><!-- end row -->

        <?php } ?>

    </div><!-- end container -->

</section><!-- end services_wrapper -->

<script>
	jQuery(document).ready(function ($){
		$('.sevices_slider').slick({
			autoplay: false,
			dots: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			infinite: true,
			nextArrow: $('.sevice_arrow_right'),
			prevArrow: $('.sevice_arrow_left'),
			responsive: [
                {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
                },
                {
					breakpoint: 800,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				}
			]
		});
	})
</script>