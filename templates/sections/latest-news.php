<?php
$bg_colour = get_sub_field('background_colour');
?>
<section class="latest_news_wrapper" bg-colour="<?php echo $bg_colour; ?>">

    <div class="container">

        <div class="row">

            <div class="col_intro col-lg-10 offset-lg-1">

                <?php $subheading = get_sub_field('subheading'); ?>
                <?php if ($subheading) { ?>
                    <h3 class="subheading mb-0" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $subheading; ?></h3>
                <?php } ?>

                <?php $heading = get_sub_field('heading'); ?>
                <?php if ($heading) { ?>
                    <h2 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $heading; ?></h2>
                <?php } ?>

            </div><!-- end col-6 -->

        </div><!-- end row -->

        <div class="row d-flex align-items-stretch">

            <?php $query = new WP_Query( array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'order' => 'DESC'
            ) ); ?>
            <?php if ( $query->have_posts() ) { ?>
                <?php $count = 0; ?>
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                    <?php if ($count == 0) { ?>
                        <div class="col-xl-6 col-lg-12">

                            <?php get_template_part('templates/post-featured'); ?>

                        </div><!-- end col-8 -->

                    <?php } else { ?>
                        <div class="col-xl-3 col-lg-6">

                            <?php get_template_part('templates/post-card'); ?>

                        </div><!-- end col-3 -->
                    <?php } ?>

                    <?php $count++; ?>
                <?php endwhile; wp_reset_postdata(); ?>

            <?php } ?>

        </div><!-- end row -->

    </div><!-- end container -->

</section><!-- end latest_news_wrapper -->
