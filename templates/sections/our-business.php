<div class="our_business_wrapper">
    <div class="container">
        <div class="row">
            <div class="col_content col-lg-3 offset-lg-1">
                <?php $heading = get_sub_field('heading'); ?>
                <?php if ($heading) { ?>
                    <h4 class="heading"><?php echo $heading; ?></h4>
                <?php } ?>
                <?php $body = get_sub_field('body'); ?>
                <?php if ($body) { ?>
                    <p class="max-para"><?php echo $body; ?></p>
                <?php } ?>
            </div>
            <div class="icon_block col-lg-6 ">
                <div class="row">

                <?php if( have_rows('icon_blocks') ):
                    while( have_rows('icon_blocks') ) : the_row(); ?>
                        <div class="single_icon_block col-xl-4 offset-xl-2 col-lg-5 offset-lg-1">
                            <?php $image = get_sub_field('icon');
                            if( !empty( $image ) ): ?>
                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            <?php endif; ?>
                            <?php $body = get_sub_field('body'); ?>
                            <?php if ($body) { ?>
                                <p class="body large-para"><?php echo $body; ?></p>
                            <?php } ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                            </div>
            </div>
        </div>
    </div>
</div>