<section class="testimonials_wrapper">


    <div class="content-wrapper">

        <div class="container">

            <div class="row">

                <div class="col_content col-lg-10 offset-lg-1">

                    <div class="inner">

                        <?php
                        $subheading = get_sub_field('subheading');
                        if (empty($subheading) && null !== get_field('testimonials_subheading', 'option')) {
                            $subheading = get_field('testimonials_subheading', 'option');
                        }

                        $heading = get_sub_field('heading');
                        if (empty($heading) && null !== get_field('testimonials_heading', 'option')) {
                            $heading = get_field('testimonials_heading', 'option');
                        }

                        $body = get_sub_field('body');
                        if (empty($body) && null !== get_field('testimonials_body', 'option')) {
                            $body = get_field('testimonials_body', 'option');
                        }
                        ?>

                        <?php if (!empty($subheading)) { ?>
                            <h3 class="subheading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $subheading; ?></h3>
                        <?php } ?>

                        <?php if (!empty($heading)) { ?>
                            <h2 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $heading; ?></h2>
                        <?php } ?>

                        <?php if (!empty($body)) { ?>
                            <div class="body" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                                <?php echo $body; ?>
                            </div>
                        <?php } ?>


                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="testi-wrapper">

        <div class="container">

            <div class="row">

                <div class="col_content col-lg-12">

                    <?php $query = new WP_Query( array(
                        'post_type' => 'client',
                        'posts_per_page' => -1,
                        'order' => 'DESC'
                    ) ); ?>
                    <?php if ( $query->have_posts() ) { ?>
                        <div class="testimonials_carousel">

                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                                <?php global $post; ?>
                                <div class="testimonial">

                                    <div class="inner">

                                        <div class="body para-max">

                                            <?php the_content(); ?>

                                        </div><!-- end body -->

                                        <h4><?php the_title(); ?></h4>
                                        
                                        <?php
                                        $catslist = get_the_terms($post->ID, 'client_category');
                                        if ($catslist && is_array($catslist)) {
                                            $n = count($catslist); ?>
                                            <p class="testi-cat small-para"><?php foreach ($catslist as $i => $cat) { if (($i+1) != $n) {echo ($cat->name.', ');} else {echo $cat->name;} } ?></p>
                                        <?php } else {
                                            $n = 0;
                                        } ?>

                                    </div><!-- end inner -->

                                </div><!-- end news -->

                            <?php endwhile; wp_reset_postdata(); ?>

                        </div><!-- end testimonials_carousel -->


                    <?php } ?>

                </div><!-- end col-6 -->


            </div><!-- end row -->

        </div><!-- end container -->

    </div>
</section><!-- end testimonials_wrapper -->


<script>
    jQuery(document).ready(function ($) {

        var $testimonials_carousel  = $('.testimonials_carousel');

        $testimonials_carousel.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            infinite: false,
            fade: false,
            arrows: true,
            prevArrow:'<a href="" class="left"></a>',
            nextArrow:'<a href="" class="right"></a>',
            speed: 1000,
            lazyLoad: 'ondemand',
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1
                    }
                }

            ]
        });

    });
</script>

<script>
//make carousel wrappers height equal to greatest height (adaptive height does not work for multi slide)
jQuery(document).ready(function() {
    function adjustCarouselWrappers(){
          var testiMaxHeight = 0;

          jQuery(".testimonial .inner").each(function() {
            if (jQuery(this).height() > testiMaxHeight) {
            jQuery(this).height('auto');
              testiMaxHeight = jQuery(this).height();
            }
        }).height(testiMaxHeight);

    }

    adjustCarouselWrappers();

    //change on re-size
    var to = true,
    throttle = function(func, delay){
        if (to) {
        window.clearTimeout(to);
        }
        to = window.setTimeout(func, delay);
    };

    window.onresize = function(){
        throttle(function(){
            adjustCarouselWrappers();
        },500);
    };


});
</script>
