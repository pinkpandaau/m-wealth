<?php $bg_colour = get_sub_field('background_colour'); ?>
<section class="the_journey_wrapper" bg-colour="<?php echo $bg_colour; ?>">

    <div class="container">

        <div class="row">

            <div class="col_content col-xl-6 col-md-8">

                <?php $subheading = get_sub_field('subheading'); ?>
                <?php if ($subheading) { ?>
                    <h3 class="subheading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $subheading; ?></h3>
                <?php } ?>

                <?php $heading = get_sub_field('heading'); ?>
                <?php if ($heading) { ?>
                    <h2 class="heading" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800"><?php echo $heading; ?></h2>
                <?php } ?>

                <?php $body = get_sub_field('body'); ?>
                <?php if ($body) { ?>
                    <div class="body large-para" data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800">
                        <?php echo $body; ?>
                    </div>
                <?php } ?>

            </div><!-- end col-6 -->

            <div class="col-xl-6 col-md-4 col_content_right">

                <?php $small_link = get_sub_field('link');
                if( $small_link ):
                    $small_link_url = $small_link['url'];
                    $small_link_title = $small_link['title'];
                    $small_link_target = $small_link['target'] ? $small_link['target'] : '_self';
                    ?>
                    <a data-aos="fade-in" data-aos-offset="150" data-aos-delay="350" data-aos-duration="800" class="link arrow link-right-chevron" href="<?php echo esc_url( $small_link_url ); ?>" target="<?php echo esc_attr( $small_link_target ); ?>">
                        <?php echo esc_html( $small_link_title ); ?>
                        <svg xmlns="http://www.w3.org/2000/svg" width="3.562" height="5.96" viewBox="0 0 3.562 5.96">
                            <path id="Path_193" data-name="Path 193" d="M24.331,11.256l2.51-2.635-2.51-2.635" transform="translate(-23.969 -5.641)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                        </svg>
                    </a>
                <?php endif; ?>

            </div>

        </div>

    </div>

    <div class="container">

        <?php if( have_rows('steps', 'option') ): ?>

                <div class="the_journey">

                    <div class="row">

                        <?php $i = 1;?>

                            <?php while ( have_rows('steps', 'option') ) : the_row(); ?>

                                <?php if($i == 1 || $i == 2 || $i == 3){
                                    $class='col-lg-4';
                                    $headerClass = "reg";
                                } else if($i == 4){
                                    $class="col-lg-3 offset-lg-1 order-lg-5";
                                    $headerClass = "reg";
                                }
                                else{
                                    $class="col-lg-3 offset-lg-2";
                                    $headerClass = "orange";
                                }?>

                                <div class="step <?php echo $class;?> step-<?php echo $i;?>">

                                    <?php $the_image_id = get_sub_field('icon'); ?>
                                    <?php if($the_image_id) {
                                        $image_url = wp_get_attachment_image_src( $the_image_id, 'full' )[0];
                                        $alt = get_post_meta( $the_image_id, '_wp_attachment_image_alt', true ); ?>

                                        <img src="<?php echo $image_url; ?>" alt="<?php echo esc_attr($alt); ?>" class="img-responsive" />

                                    <?php } ?>

                                    <?php if(get_sub_field('heading')) { ?> 
                                        <h3 class="heading <?php echo $headerClass;?>"><?php the_sub_field('heading'); ?></h3>
                                    <?php } ?>

                                    <?php if(get_sub_field('body')) { ?> 
                                        <div class="large-para">
                                            <?php the_sub_field('body'); ?>
                                        </div>
                                    <?php } ?>

                                </div><!-- end step -->

                                <img src='<?php echo get_template_directory_uri(); ?>/dist/images/dashed-lines.svg' alt="dashed lines" class="img-responsive mx-auto mobile-journey-lines mobile-journey-line-<?php echo $i;?>">

                                <?php $i++;?>

                            <?php endwhile; ?>

                    </div>

                </div><!-- end the_journey -->

        <?php endif; ?>

    </div>

</section><!-- end the_journey_wrapper -->
