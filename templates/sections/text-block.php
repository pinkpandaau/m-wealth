<div class="text_block">
    <div class="container">
        <div class="row">

            <?php $subheading = get_sub_field('sub_heading'); ?>
            <?php if ($subheading) { ?>
                <h3 class="subheading orange col-lg-10 offset-lg-1"><?php echo $subheading; ?></h3>
            <?php } ?>
            <?php $left_body = get_sub_field('left_body'); ?>
            <?php if ($left_body) { ?>
                <div class="left_body col-lg-4 offset-lg-1 mb-0"><?php echo $left_body; ?></div>
            <?php } ?>
            <?php $right_body = get_sub_field('right_body'); ?>
            <?php if ($right_body) { ?>
                <p class="large-para offset-lg-1 col-lg-4 mb-0 px-lg-0"><?php echo $right_body; ?></p>
            <?php } ?>
        </div>
    </div>
</div>