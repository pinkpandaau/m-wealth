<div class="our_team" id="our-people">
    <div class="container">
        <div class="row">
            <div class="top_wrap col-lg-6 offset-lg-3 text-center">
                <?php $title = get_sub_field('title');
                if (!$title) {
                    $title =  get_field('team_title', 'option');
                }

                ?>
                <?php if ($title) { ?>
                    <h2 class="heading"><?php echo $title; ?></h2>
                <?php } ?>

                <?php $body = get_sub_field('body');
                if (!$body) {
                    $body =  get_field('team_body', 'option');
                }

                ?>
                <?php if ($body) { ?>
                    <div class="body">
                        <?php echo $body; ?>
                    </div>
                <?php } ?>
            </div>
            <?php if (get_sub_field('layout') == 'grid') { ?>
                <div class="team_wrapper col-xl-10 offset-xl-1">
                    <div class="">

                        <div class="team_carousel">
                        
                            <?php $team = get_sub_field('team');
                            if( $team ) { ?>

                                <?php foreach( $team as $post ):

                                    setup_postdata($post); ?>

                                    <?php get_template_part('templates/team-tile'); ?>
                                    
                                <?php endforeach; ?>

                                <?php wp_reset_postdata(); ?>

                            <?php } else { ?>

                                <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'team',
                                    'posts_per_page' => 10,
                                    'order' => 'ASC'
                                ) ); 

                                if ( $query->have_posts() ) {   ?>

                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                                        <?php get_template_part('templates/team-tile'); ?>

                                    <?php endwhile; wp_reset_postdata(); ?>

                                <?php } ?>

                            <?php }  ?>
                        
                        </div><!-- end team_carousel -->
                        

                    </div>

                    <script>
                        jQuery(document).ready(function ($) {

                            var $team_carousel  = $('.team_carousel');

                            $team_carousel.slick({
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                autoplay: true,
                                infinite: true,
                                fade: false,
                                arrows: true,
                                prevArrow:'<a href="" class="left"></a>',
                                nextArrow:'<a href="" class="right"></a>',
                                speed: 1000,
                                dots: true,
                                lazyLoad: 'ondemand',
                                adaptiveHeight: true,
                                responsive: [
                                    {
                                        breakpoint: 1200,
                                        settings: {
                                            slidesToShow: 3
                                        }
                                    },
                                    {
                                        breakpoint: 991,
                                        settings: {
                                            slidesToShow: 2
                                        }
                                    },
                                    {
                                        breakpoint: 767,
                                        settings: {
                                            slidesToShow: 1
                                        }
                                    },
                                    {
                                        breakpoint: 575,
                                        settings: {
                                            slidesToShow: 1
                                        }
                                    }

                                ]
                            });

                        });
                    </script>

                    <?php if(get_sub_field('btn') || get_field('team_button', 'option')) { ?>      
                        <div class="row">

                            <div class="col_link col-lg-12 text-center">
                                <?php 
                                $btn = get_sub_field('btn');
                                if (!$btn) {
                                    $btn = get_field('team_button', 'option');
                                }
                                if( $btn ): 
                                    $btn_url = $btn['url'];
                                    $btn_title = $btn['title'];
                                    ?>
                                    <a class="link arrow link-right-chevron" href="<?php echo esc_url( $btn_url ); ?>">
                                        <?php echo esc_html( $btn_title ); ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="3.562" height="5.96" viewBox="0 0 3.562 5.96">
                                            <path id="Path_193" data-name="Path 193" d="M24.331,11.256l2.51-2.635-2.51-2.635" transform="translate(-23.969 -5.641)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
                                        </svg>
                                    </a>
                                <?php endif; ?>
                            </div>

                        </div>
                    <?php } ?>
                 
                </div>
            <?php } else { ?>
                <div class="team_wrapper col-xl-8 offset-xl-2">


                    <?php $team = get_sub_field('team');
                    if( $team ) { ?>

                        <?php foreach( $team as $post ):

                            setup_postdata($post); ?>
                            <div class="col-lg-12 col_team col_team_block">

                                <div class="inner">
                                    <div class="row">

                                        <div class="col_image col-lg-6">


                                            <?php
                                            $image_data = get_image_data(get_post_thumbnail_id());
                                            if (!empty($image_data)) { ?>
                                                <img
                                                    src="<?php echo $image_data['url']; ?>"
                                                    srcset="<?php echo $image_data['srcset']; ?>"
                                                    sizes="100vw"
                                                    width="<?php echo $image_data['width']; ?>"
                                                    height="<?php echo $image_data['height']; ?>"
                                                    alt="<?php echo $image_data['alt']; ?>"
                                                    class="img-responsive"
                                                />
                                            <?php } ?>


                                        </div>

                                        <div class="col_content col-lg-6">
                                            <h4 class="name"><?php the_title(); ?></h4>
                                            <?php $designation = get_field('designation'); ?>
                                            <?php if ($designation) { ?>
                                                <p class="small-para orange-light"><?php echo $designation; ?></p>
                                            <?php } ?>
                                            <div class="large-para"><?php the_content(); ?></div>
                                            <?php $role = get_field('role'); ?>
                                            <?php if ($role) { ?>
                                                <p class="role"><?php echo $role; ?></p>
                                            <?php } ?>
                                            <?php $email = get_field('email'); ?>
                                            <?php if ($email) { ?>
                                                <a class="mail blue" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        <?php endforeach; ?>

                        <?php wp_reset_postdata(); ?>

                    <?php } else { ?>

                        <?php
                        $query = new WP_Query( array(
                            'post_type' => 'team',
                            'posts_per_page' => -1,
                            'order' => 'ASC'
                        ) ); 

                        if ( $query->have_posts() ) {   ?>

                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                                <div class="col-lg-12 col_team">

                                    <div class="inner">
                                        <div class="row">

                                            <div class="col_image col-lg-6">


                                                <?php //$image_data = get_image_data(get_sub_field('field_name'));
                                                $image_data = get_image_data(get_post_thumbnail_id());
                                                if (!empty($image_data)) { ?>
                                                    <img
                                                        src="<?php echo $image_data['url']; ?>"
                                                        srcset="<?php echo $image_data['srcset']; ?>"
                                                        sizes="100vw"
                                                        width="<?php echo $image_data['width']; ?>"
                                                        height="<?php echo $image_data['height']; ?>"
                                                        alt="<?php echo $image_data['alt']; ?>"
                                                        class="img-responsive"
                                                    />
                                                <?php } ?>


                                            </div>

                                            <div class="col_content col-lg-6">
                                                <h4 class="name"><?php the_title(); ?></h4>
                                                <?php $designation = get_field('designation'); ?>
                                                <?php if ($designation) { ?>
                                                    <p class="small-para mb-0 orange-light"><?php echo $designation; ?></p>
                                                <?php } ?>
                                                <div class="large-para"><?php the_content(); ?></div>
                                                <?php $role = get_field('role'); ?>
                                                <?php if ($role) { ?>
                                                    <p class="role"><?php echo $role; ?></p>
                                                <?php } ?>
                                                <?php $email = get_field('email'); ?>
                                                <?php if ($email) { ?>
                                                    <a class="mail blue" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                                <?php } ?>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            <?php endwhile; wp_reset_postdata(); ?>

                        <?php } ?>
                        
                    <?php } ?>
                    
                </div>
            <?php } ?>
        </div>
    </div>
</div>