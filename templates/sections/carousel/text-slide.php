<div class="slide">

	<div class="inside_slide">
	
		<div class="inner">
		
		    <?php if(get_sub_field('subheading')) { ?> 
				<h3 class="subheading orange"><?php the_sub_field('subheading'); ?></h3>
			<?php } ?>

			<?php if(get_sub_field('body')) { ?> 
				<div class="body_info"><?php the_sub_field('body'); ?></div>
			<?php } ?>

			<?php $link = get_sub_field('link');
				if( $link ):
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link link-right-chevron" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
					<?php echo esc_html( $link_title ); ?>
					<svg xmlns="http://www.w3.org/2000/svg" width="3.562" height="5.96" viewBox="0 0 3.562 5.96">
						<path id="Path_193" data-name="Path 193" d="M24.331,11.256l2.51-2.635-2.51-2.635" transform="translate(-23.969 -5.641)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
					</svg>
				</a>
			<?php endif; ?>
		
		</div><!-- end inner -->
		

	</div><!-- end inside_slide -->

</div><!-- end slide -->
