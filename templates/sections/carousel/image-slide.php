<div class="slide">

	<?php $image_data = get_image_data(get_sub_field('background_image'));
	//$image_data = get_image_data(get_post_thumbnail_id());
	if (!empty($image_data)) { ?>
		<div class="bg_image">
		
		    <img
		        src="<?php echo $image_data['url']; ?>"
		        srcset="<?php echo $image_data['srcset']; ?>"
		        sizes="100vw"
		        width="<?php echo $image_data['width']; ?>"
		        height="<?php echo $image_data['height']; ?>"
		        alt="<?php echo $image_data['alt']; ?>"
		        class="img_tag_bg"
		    />

		    <?php
            $image_data = get_image_data(get_field('gradient_overlay', 'option'));
            if (!empty($image_data)) { ?>
                <img
                    src="<?php echo $image_data['url']; ?>"
                    srcset="<?php echo $image_data['srcset']; ?>"
                    sizes="100vw"
                    width="<?php echo $image_data['width']; ?>"
                    height="<?php echo $image_data['height']; ?>"
                    alt="<?php echo $image_data['alt']; ?>"
                    class="img_tag_bg image_overlay"
                />
            <?php } else { ?>
                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/carousel-overlay.png" class="img_tag_bg image_overlay" />
            <?php } ?>
		
		</div><!-- end bg_image -->
	    
	<?php } ?>
	
</div><!-- end slide -->
