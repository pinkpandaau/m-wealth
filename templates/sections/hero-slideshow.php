<?php $youtube_url = get_sub_field('youtube_url');
//$youtube_url = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';
if ($youtube_url) {
    $youtube_url = convertYoutubeUrlToEmbedUrl($youtube_url);
} ?>

<section class="hero_slideshow_wrapper">

    <?php if( have_rows('slideshow') ): ?>

        <div class="hero_slideshow">

            <?php while ( have_rows('slideshow') ) : the_row(); ?>

                <div class="hero_slide">

                    <?php $the_image_id = get_sub_field('background_image'); ?>
                    <?php if($the_image_id) {
                        $image_url = wp_get_attachment_image_src( $the_image_id, 'full' )[0]; ?>
                        <!--<div class="bg_image" style="background-image: url('<?php echo esc_url($image_url); ?>');"></div>-->
                    <?php } ?>

                    <?php $image_data = get_image_data(get_sub_field('background_image'));
                    //$image_data = get_image_data(get_post_thumbnail_id());
                    if (!empty($image_data)) { ?>
                        <img
                            src="<?php echo $image_data['url']; ?>"
                            srcset="<?php echo $image_data['srcset']; ?>"
                            sizes="100vw"
                            width="<?php echo $image_data['width']; ?>"
                            height="<?php echo $image_data['height']; ?>"
                            alt="<?php echo $image_data['alt']; ?>"
                            class="img_tag_bg"
                            style="position: absolute;width: 100%;height: 100%;-o-object-fit: cover;object-fit: cover;left: 0;top: 0;"
                        />
                    <?php } ?>


                    <?php
                    $image_data = get_image_data(get_field('gradient_overlay', 'option'));
                    if (!empty($image_data)) { ?>
                        <img
                            src="<?php echo $image_data['url']; ?>"
                            srcset="<?php echo $image_data['srcset']; ?>"
                            sizes="100vw"
                            width="<?php echo $image_data['width']; ?>"
                            height="<?php echo $image_data['height']; ?>"
                            alt="<?php echo $image_data['alt']; ?>"
                            class="img_tag_bg image_overlay"
                            style="position: absolute;width: 100%;height: 100%;-o-object-fit: cover;object-fit: cover;left: 0;top: 0;"
                        />
                    <?php } else { ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/carousel-overlay.png" class="img_tag_bg image_overlay" />
                    <?php } ?>


                    <div class="container thin-container">

                        <div class="row">

                            <div class="col-lg-8 col-xl-6 col-md-12 col_content">

                                <div class="inner">
                                    <?php if(get_sub_field('body')) { ?> 
                                        <div class="body_text">
                                            <?php the_sub_field('body'); ?>
                                        </div>
                                    <?php } ?>

                                    <div class="services_dropdown">

                                        <?php if (get_sub_field('show_services')) { ?>

                                            <?php if(get_sub_field('services_heading')) { ?> 
                                                <h4 class="subheading"><?php the_sub_field('services_heading'); ?></h4>
                                            <?php } ?>

                                            <div class="select-wrap">
                                                <select id="dynamic-select">
                                                    <?php $query = new WP_Query( array(
                                                        'post_type' => 'service',
                                                        'posts_per_page' => -1,
                                                        'order' => 'ASC'
                                                    ) ); ?>
                                                    <?php if ( $query->have_posts() ) { ?>

                                                        <option value="" disabled selected>Select</option>

                                                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                                                            <option value="<?php echo the_permalink(); ?>"><?php the_title(); ?></option>

                                                        <?php endwhile; wp_reset_postdata(); ?>

                                                    <?php } ?>

                                                </select>
                                            </div>

                                        <?php } ?>

                                    </div><!-- end services_dropdown -->

                                </div><!-- end inner -->

                            </div><!-- end col-6 col_content -->

                            <?php if ($youtube_url) { ?>
                                <div class="col-lg-4 col-xl-6 col_video">
                                
                                    <div class="play_wrapper"  data-izimodal-open="#modal-youtube">
                                    
                                        <img src='<?php echo get_template_directory_uri(); ?>/dist/images/play-button.png' alt="Play"width="100" height="100">
                                    
                                    </div><!-- end play_wrapper -->

                                </div><!-- end col-lg-4 col-xl-6 col_video -->
                            <?php } ?>

                        </div><!-- end container -->

                    </div><!-- end row -->

                </div><!-- end hero_slide -->

            <?php endwhile; ?>

        </div><!-- end hero_slideshow -->

    <?php endif; ?>

</section><!-- end hero_slideshow_wrapper -->

<?php if ($youtube_url) { ?>
    <div id="modal-youtube" class="modais" data-izimodal-transitionin="fadeInDown" data-izimodal-title="M-Wealth" data-izimodal-iframeURL="<?php echo $youtube_url; ?>"></div>
<?php } ?>

<script>
    jQuery(document).ready(function ($) {

        var $hero_slideshow  = $('.hero_slideshow');

        $hero_slideshow.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            infinite: true,
            fade: false,
            arrows: false,
            prevArrow:'<a href="" class="left"></a>',
            nextArrow:'<a href="" class="right"></a>',
            speed: 1000,
            lazyLoad: 'ondemand',
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        autoplay: false
                    }
                }
            ]
        });

        <?php if ($youtube_url) { ?>
            $(".modais").iziModal({
                history: false,
                iframe : true,
                fullscreen: true,
                headerColor: '#192d4b',
                group: 'group1',
                loop: true
            });
        <?php } ?>


    });
</script>

<script>
    jQuery(function($){
      $('#dynamic-select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
