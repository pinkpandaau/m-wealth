<div class="title_bar">
    <div class="container">
        <div class="row">
            <h3 class="heading text-center col-lg-1">Services</h3>
            <?php
            $query = new WP_Query( array(
                'post_type' => 'service',
                'posts_per_page' => 6,
                'order' => 'ASC'
            ) ); 

            global $wp_query;
            $current_id = $wp_query->get_queried_object_id();

            if ( $query->have_posts() ) {   ?>
                <div class="services_topics_slick col-lg-11">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        
                        <?php if( $current_id == get_the_ID() ) { ?>
                            <div class="service_topics active">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
                        <?php } else { ?>
                            <div class="service_topics">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
                        <?php } ?>
                    <?php endwhile; wp_reset_postdata(); ?>

                </div><!-- end services_topics_slick -->

            <?php } ?>
        </div>
    </div>
</div>

<div class="service_wrapper">
    <div class="container">
        <div class="row">

            <div class="col_content col-lg-7 offset-lg-1">
                <?php the_content(); ?>
            </div>

            <div class="form_wrapper col-lg-4">

                <?php $services_form_heading = get_field('services_form_heading', 'option');
                $services_form_body = get_field('services_form_body', 'option');
                if ($services_form_heading) { ?>
                    <h4><?php echo $services_form_heading; ?></h4>
                <?php } ?>
                <?php if ($services_form_body) { ?>
                    <div class="body large-para"><?php echo $services_form_body; ?></div>
                <?php } ?>

                <?php 
                    $formID = '4';
                    if ($formID) {
                        echo do_shortcode('[gravityform id="'.$formID.'" title="false" description="false" ajax="true"]');
                    }
                ?>
            </div>

        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {

        var $services_topics_slick  = $('.services_topics_slick');

        $services_topics_slick.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            infinite: false,
            fade: false,
            arrows: true,
            prevArrow: false,
            nextArrow: false,
            speed: 1000,
            lazyLoad: 'ondemand',
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2
                    }
                },

                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1
                    }
                }

            ]
        });

    });
</script>