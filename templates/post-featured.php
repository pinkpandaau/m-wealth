<article class="featured-article">

	<?php //$image_data = get_image_data(get_sub_field('field_name'));
	$image_data = get_image_data(get_post_thumbnail_id());
	if (!empty($image_data)) { ?>
		<div class="bg_image">
		
		    <img
		        src="<?php echo $image_data['url']; ?>"
		        srcset="<?php echo $image_data['srcset']; ?>"
		        sizes="100vw"
		        width="<?php echo $image_data['width']; ?>"
		        height="<?php echo $image_data['height']; ?>"
		        alt="<?php echo $image_data['alt']; ?>"
		        class="img_tag_bg"
		    />
		    <div class="overlay">
			</div>
		
		</div><!-- end bg_image -->
	    
	<?php } ?>


	<div class="inner">

		<span>Featured article</span>
		 <a href="<?php the_permalink(); ?>"><h4 class="medium-heading"><?php the_title(); ?></h4></a>
		 <div class="float_center">
			 <?php the_excerpt();?>
			 <a class="more small-para" href="<?php echo the_permalink();?>">
				 read more <svg xmlns="http://www.w3.org/2000/svg" width="20.572" height="12.54" viewBox="0 0 20.572 12.54">
				   <g id="Group_2195" data-name="Group 2195" transform="translate(0 0.345)">
					 <line id="Line_1" data-name="Line 1" x1="19.882" transform="translate(0 5.925)" fill="none" stroke="#FFFFFF" stroke-miterlimit="10" stroke-width="1"/>
					 <path id="Path_1" data-name="Path 1" d="M24.331,17.836l5.643-5.925L24.331,5.986" transform="translate(-10.092 -5.986)" fill="none" stroke="#FFFFFF" stroke-miterlimit="10" stroke-width="1"/>
				   </g>
				 </svg>
			 </a>
		 </div>

	</div><!-- end inner -->

</article>
