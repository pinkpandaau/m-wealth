<footer class="primary-footer">

    <div class="signup_form_wrapper form_wrapper">

        <div class="container thin-container">

            <div class="row">

                <div class="col-12">

                    <div class="inner">

                        <h4 class="blue large-para"><?php the_field('footer_signup_text', 'option'); ?></h4>
                        <?php
                        $formID = get_field('sign_up_form_id', 'option');
                        if ($formID) {
                            echo do_shortcode('[gravityform id="'.$formID.'" title="false" description="false" ajax="true"]');
                        } ?>

                    </div><!-- end inner -->

                </div><!-- end col-lg-10 offset-lg-1 -->

            </div><!-- end row -->

        </div><!-- end container -->


    </div><!-- end signup_form_wrapper -->

    <div class="container thin-container">

        <div class="row top_row">

            <div class="col-lg-3 col-md-12 col_contact_info">
                <div class="footer_phone">
                    <a href="<?php echo clean_phone(get_field('phone', 'options')); ?>"><?php echo get_field('phone', 'options'); ?></a>
                </div><!-- footer_phone -->
                <div class="footer_address">
                    <a href="https://goo.gl/maps/RjevGev7Qk2stWBp9" target="_blank"><?php echo get_field('address', 'options'); ?></a>
                </div><!-- footer_address -->

            </div>

            <div class="col-lg-7 col-md-12 col_links">

                <?php
                    if (has_nav_menu('footer_links_1')) :
                        wp_nav_menu(['theme_location' => 'footer_links_1', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav', 'menu_id' => 'footer-menu']);
                    endif;
                ?>

            </div><!-- end col-lg-4 -->

            <div class="col-lg-2 col-md-12 col_socials">

                <?php if( have_rows('social_media_accounts', 'option') ): ?>
                    <div class="links_wrapper">

                        <?php while( have_rows('social_media_accounts', 'option') ): the_row(); ?>

                            <?php
                                $icon = get_sub_field('social_media_icon');
                                $title = get_sub_field('social_media_title');
                                $url = get_sub_field('social_media_link');
                            ?>
                            <a href="<?php echo $url; ?>" target="_blank">
                                <i class="fa fa-<?php echo $icon; ?>"></i>
                            </a>

                        <?php endwhile; ?>

                    </div><!-- end social_wrapper -->

                <?php endif; ?>

            </div><!-- end col-md-3 col-twins -->


        </div><!-- end row -->

        <div class="row bottom_row">

            <div class="col-lg-3">

                <div class="inner">
                    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                        <img src='<?php echo get_template_directory_uri(); ?>/dist/images/logo/m-wealth-logo-light.svg' alt="M-Wealth Footer Logo" id="footer-logo" class="img-responsive">
                    </a>
                </div>

            </div><!-- end col-lg-3 offset-lg-1 -->

            <div class="col-lg-9">

                <?php
                    if (has_nav_menu('footer_links_2')) :
                        wp_nav_menu(['theme_location' => 'footer_links_2', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav', 'menu_id' => 'footer-terms']);
                    endif;
                ?>

            </div><!-- end col-lg-7 -->

        </div><!-- end row -->


    </div><!-- end container -->

    <div class="terms-wrapper">
        <div class="container thin-container">
            <div class="row">
                <div class="col-12">
                    <?php the_field('terms_and_conditions', 'option'); ?>
                </div>
            </div>
        </div>
    </div>




</footer>

<script>
    jQuery(document).ready(function ($) {
        AOS.init({
            'once': true
        });

        document.addEventListener('aos:in', function(detail) {
            setTimeout(function() {
                $(detail).removeAttr("data-aos");
                $(detail).removeClass("aos-init aos-animate");
            }, 1000);
        });
    });

</script>
