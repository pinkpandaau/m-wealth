<div class="col_team">

    <div class="inner">

        <div class="wrapper text-center">

            <?php if(has_post_thumbnail()) { ?>
                <?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0]; ?>
                <img src="<?php echo $featured_image; ?>" alt="">
            <?php } ?>

            <div class="content">

                <h4 class="max-para"><?php the_title(); ?></h4>
                <?php $designation = get_field('designation'); ?>
                <?php if ($designation) { ?>
                    <p class="small-para mb-0 orange-light"><?php echo $designation; ?></p>
                <?php } ?>

            </div>

        </div>

    </div>

</div>