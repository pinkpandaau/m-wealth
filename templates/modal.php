<div class="modal_wrapper" style="display: none;">

	<div class="close_icon">
        <span></span>
    </div>

    <div class="inner">
    
        <div class="row">

            <?php
            $modal_heading = get_field('modal_heading', 'option');
            if ($modal_heading) { ?>
                <div class="col-12 col_intro">
                
                    <h3><?php echo $modal_heading; ?></h3>
                
                </div><!-- end col-12 -->
            <?php } ?>

        	<div class="col-lg-12 col_content">
            
                
            
            </div><!-- end classname -->
        
        </div><!-- end row -->
    
    </div><!-- end inner -->

</div><!-- end modal_wrapper -->		

<script>

    jQuery(document).ready(function($) {
    	$(".modal_wrapper").iziModal({
			width: 800,
			radius: 30,
			onOpening: function() {
				/*jQuery('.modal_wrapper .gfield_select').select2({
                    minimumResultsForSearch: -1,
                    theme: "gform-dropdown",
                    dropdownParent: jQuery('.modal_wrapper')
                });*/
			},
			onClosing: function() {
				/*jQuery('.eckermann_assist_wrapper .gfield_select').select2({
					minimumResultsForSearch: -1,
					theme: "gform-dropdown",
				});*/
			}
		});

        //$('.modal_wrapper').iziModal('open');

		$( ".trigger" ).click(function(event) {
			event.preventDefault();
			$('.modal_wrapper').iziModal('open');
			$('input').blur();
		});

		

		$( ".modal_wrapper .close_icon" ).click(function(event) {
			event.preventDefault();
			$('.modal_wrapper').iziModal('close');
		});

 	});

</script>