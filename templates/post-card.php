<article class="reg-article">

	<?php //$image_data = get_image_data(get_sub_field('field_name'));
	$image_data = get_image_data(get_post_thumbnail_id());
	if (!empty($image_data)) { ?>
		<a href="<?php the_permalink(); ?>">
		    <img
		        src="<?php echo $image_data['url']; ?>"
		        srcset="<?php echo $image_data['srcset']; ?>"
		        sizes="100vw"
		        width="<?php echo $image_data['width']; ?>"
		        height="<?php echo $image_data['height']; ?>"
		        alt="<?php echo $image_data['alt']; ?>"
		        class="img_tag_bg"
		    />
		</a>
	<?php } ?>


	<div class="inner">

	    <a href="<?php the_permalink(); ?>"><h4 class="medium-heading"><?php echo wp_trim_words( get_the_title(), 10 ); ?></h4></a>

	</div><!-- end inner -->

	<div class="link-wrapper">
		<a href="<?php the_permalink(); ?>" class="link_extended">
			Read more <svg xmlns="http://www.w3.org/2000/svg" width="20.572" height="12.54" viewBox="0 0 20.572 12.54">
			  <g id="Group_2195" data-name="Group 2195" transform="translate(0 0.345)">
			    <line id="Line_1" data-name="Line 1" x1="19.882" transform="translate(0 5.925)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
			    <path id="Path_1" data-name="Path 1" d="M24.331,17.836l5.643-5.925L24.331,5.986" transform="translate(-10.092 -5.986)" fill="none" stroke="#192d4b" stroke-miterlimit="10" stroke-width="1"/>
			  </g>
			</svg>
		</a>
	</div>

</article>
