<?php if ( !empty( get_the_content() ) ){ ?>
	
	<div class="default_page_wrapper container">

	    	
			<?php while (have_posts()) : the_post(); ?>
				<?php //get_template_part('templates/page', 'header'); ?>
				<?php get_template_part('templates/content', 'page'); ?>
			<?php endwhile; ?>
		   

	</div><!-- end default_page_wrapper container -->

<?php } ?>

<?php get_template_part('templates/flexible-content'); ?>