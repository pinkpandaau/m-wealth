<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/assets.php',                               // Scripts and stylesheets
    'lib/extras.php',                               // Custom functions
    'lib/setup.php',                                // Theme setup
    'lib/titles.php',                               // Page titles
    'lib/wrapper.php',                              // Theme wrapper class
    'lib/customizer.php',                           // Theme customizer
    'lib/custom/pp-whitelabel.php',                 // White label functions
    'shortcodes/shortcodes-init.php',               // Register shortcodes
    'posttypes/posttypes-init.php',                 // Register custom post types
    'lib/custom/pp-siteoptions.php',                // Register ACF Site options
    'lib/custom/pp-acf.php'                         // Basic ACF options
];

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

if( function_exists('acf_add_local_field_group') ) {
    function acf_redirect_mode() {
        $redirect = get_field('site_mode', 'option');
        $redirect_url = get_field('redirect_url', 'option');

        if (!$redirect && $redirect_url && !is_user_logged_in() && !is_admin() && $GLOBALS['pagenow'] !== 'wp-login.php') {
            wp_redirect($redirect_url);
            exit;
        }
    }
    add_action ('wp_loaded', 'acf_redirect_mode');
    // ACF google maps
    function my_acf_init() {
        acf_update_setting('google_api_key', 'AIzaSyAK0enAdFFj094muJNhEb-ngRxJSXzlAig');
    }
    add_action('acf/init', 'my_acf_init');
}


function add_custom_fonts() {
  echo "<style>";
  echo returnFont("HarmoniaSans-Bold");
  echo returnFont("HarmoniaSans-SemiBold");
  echo returnFont("HarmoniaSans-Regular");
  echo returnFont("HarmoniaSans-Light");
  echo "</style>";
}
add_action( 'wp_enqueue_scripts', 'add_custom_fonts' );

function returnFont($font_name) {

  $def_stack = '@font-face {
  font-family: "{fontname}";
  font-style: normal;
  font-weight: normal;
  src: url("{themedir}/dist/fonts/{fontname}.eot");
  src: url("{themedir}/dist/fonts/{fontname}.woff?#iefix") format("embedded-opentype"), url("{themedir}/dist/fonts/{fontname}.ttf") format("truetype"), url("{themedir}/dist/fonts/{fontname}.svg?#{fontname}") format("esvg"); }';

  $def_stack = str_replace('{themedir}', get_template_directory_uri(), $def_stack);

  return str_replace('{fontname}', $font_name, $def_stack);
}


add_image_size( 'rect-sm', 448, 300, true );
add_image_size( 'rect-md', 654, 300, true );


function getBreadcrumbBackground($post) {

    $def_breadcrumb = get_field('default_breadcrumb_background', 'option');
    $page_breadcrumb = get_field('breadcrumb_background', $post->ID);
    $blog_breadcrumb = get_field('breadcrumb_background', 55);


    if (is_home()) {
      return wp_get_attachment_image_src( $blog_breadcrumb, 'full' )[0];
    } else {
      if ($page_breadcrumb) {
        return wp_get_attachment_image_src( $page_breadcrumb, 'full' )[0];
      } else {
        if (has_post_thumbnail( $post->ID ) ) {
            return wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0];
        } else if ($def_breadcrumb) {
            return wp_get_attachment_image_src( $def_breadcrumb, 'full' )[0];
        }
      }
    }
}

function get_breadcrumb_title() {
    if (is_search()) {
        return 'Search';
    } else if (is_home()) {
        return 'Blog';
    } else if (is_404()) {
        return 'Page not found';
    } else if (get_field('heading_override')) {
        return get_field('heading_override');
    } else if (is_category()) {
        return get_category( get_query_var( 'cat' ) )->name;
    } else if (is_singular('post')) {
        return get_the_title();
    } else {
        return get_the_title();
    }
}

 function wpse_custom_menu_order( $menu_ord ) {
    if ( !$menu_ord ) return true;

    return array(
        'index.php', // Dashboard
        'theme-general-settings', // Website Options
        'edit.php?post_type=page', // Pages
        'edit.php?post_type=service', //
        'edit.php?post_type=client', //
        'edit.php?post_type=team', //
        'edit.php', // Posts
        'edit.php?post_type=usp', //

        'gf_edit_forms', // Forms

        'separator1', // First separator


        'upload.php', // Media

        'link-manager.php', // Links
        'edit-comments.php', // Comments

        'separator2', // Second separator
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    );
}
add_filter( 'custom_menu_order', 'wpse_custom_menu_order', 10, 1 );
add_filter( 'menu_order', 'wpse_custom_menu_order', 10, 1 );



function remove_posts_menu() {
    //remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'remove_posts_menu');

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
    if ( isset($_GET['post']) || isset($_POST['post_ID']) ) {
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
        if( !isset( $post_id ) ) return;
        $pages = array("Home", "About", "Courts we attend", "General Advice", "Appeals");
        $excluded_template_slugs = array(
            'template-t1.php',
            'template-intro-text.php',
            'template-contact.php',
            'template-intro-block.php',
            'template-conversion-template.php'
        );

        $post = get_post($post_id);
        if(in_array(get_the_title($post_id), $pages) || $post->post_parent || in_array(basename( get_page_template_slug($post_id)), $excluded_template_slugs)){
          remove_post_type_support('page', 'editor');
        }
    }
}

function convertLinkToEmbed($videoLink, $width, $height, $frameID = "", $slider = false)
{
    $embed = '';
    if (preg_match('/https:\/\/(?:www.)?(youtube).com\/watch\\?v=(.*?)/', $videoLink)) {
        $video_id = explode("?v=", $videoLink); // For videos like http://www.youtube.com/watch?v=...
        if (empty($video_id[1]))
            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

        $video_id = explode("&", $video_id[1]); // Deleting any other params
        $video_id = $video_id[0];
        if($slider) {
            $embed = "<div id=\"" . $frameID . "\" class='video-element' data-id=".$frameID." data-width=".$width." data-height=".$height." data-videoid=".$video_id."></div>";
        } else {
            $embed = "<div id=\"" . $frameID . "\"></div><script> playerInfoList.push({id:'".$frameID."',height:'".$height."',width:'".$width."',videoId:'".$video_id."'}); console.log(playerInfoList); </script>";
        }
    }
    if (preg_match('/https:\/\/vimeo.com\/(\\d+)/', $videoLink, $regs)) {
        $embed = '<iframe src="https://player.vimeo.com/video/' . $regs[1] . '?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="' . $width . '" height="' . $height . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
    }
    return $embed;
}


function get_excerpt_trim($num_words='20', $more='..'){

    $excerpt = get_field('intro_text');
    if (!$excerpt) {
        $excerpt = get_the_excerpt();
    }

    $excerpt = wp_trim_words( $excerpt, $num_words , $more );
    return $excerpt;
}

//modify default style of Gravity Forms Submit Button -
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='btn aos-init aos-animate' id='gform_submit_button_{$form['id']}'>SUBMIT</button>";
}


/*function phone_validation( $result, $value, $form, $field ) {
    // If field parameter name is phone_field
    if($field['inputName'] == 'phone_field' && $value != '') {
        $validateNumber = "/^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/";
        if(!preg_match($validateNumber, $value)) {
            $result['is_valid'] = false;
            $result['message'] = 'Please enter a valid phone number';
        }
    }
    return $result;
}
//The following declaration targets field 16 in form 2
add_filter( 'gform_field_validation', 'phone_validation', 10, 4 );*/


function js_pagination($pages = '', $range = 2)
{  
    $showitems = ($range * 2)+1;  

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }   

    if(1 != $pages)
    {
        echo "<nav class='s-pagination js-scroll hidden show'><ul class='s-pagination__list'>";
            if($paged == 1) {
                echo "<li class='s-pagination__item'><a href='".get_pagenum_link($paged - 1)."' class='s-pagination__link disable'><svg xmlns='http://www.w3.org/2000/svg' ><g data-name='Component 70 – 1' fill='none' stroke='#192d4b' stroke-miterlimit='10'><path data-name='Line 100' d='M.69 6.27h19.881'/><path data-name='Path 7929' d='M6.333 12.195L.69 6.27 6.333.345'/></g></svg></a></li>";
            } else { 
                echo "<li class='s-pagination__item'><a href='".get_pagenum_link($paged - 1)."' class='s-pagination__link'><svg xmlns='http://www.w3.org/2000/svg' ><g data-name='Component 70 – 1' fill='none' stroke='#192d4b' stroke-miterlimit='10'><path data-name='Line 100' d='M.69 6.27h19.881'/><path data-name='Path 7929' d='M6.333 12.195L.69 6.27 6.333.345'/></g></svg></a></li>";
            }

            for ($i=1; $i <= $pages; $i++)
            {
                if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
                {
                    echo ($paged == $i)? "<li class='s-pagination__item active'><a href='".get_pagenum_link($i)."' class='s-pagination__link'>".$i."</a></li>" : "<li class='s-pagination__item'><a href='".get_pagenum_link($i)."' class='s-pagination__link'>".$i."</a></li>";
                }
            }
            if($pages == $paged) {
                echo "<li class='s-pagination__item'><a href='".get_pagenum_link($paged + 1)."' class='s-pagination__link disable'><svg xmlns='http://www.w3.org/2000/svg'><g data-name='Component 71 – 1' fill='none' stroke='#192d4b' stroke-miterlimit='10'><path data-name='Line 1' d='M19.882 6.27H0'/><path data-name='Path 1' d='M14.239 12.195l5.643-5.925L14.239.345'/></g></svg></a></li>";  
            } else { 
                echo "<li class='s-pagination__item'><a href='".get_pagenum_link($paged + 1)."' class='s-pagination__link'><svg xmlns='http://www.w3.org/2000/svg'><g data-name='Component 71 – 1' fill='none' stroke='#192d4b' stroke-miterlimit='10'><path data-name='Line 1' d='M19.882 6.27H0'/><path data-name='Path 1' d='M14.239 12.195l5.643-5.925L14.239.345'/></g></svg></a></li>";  
            }
        echo "</ul></nav>";
    }
}


function get_image_data($field_value, $size = 'full') {
    if (empty($field_value)) {
        return array();
    }

    // Check if the field value is an image ID (from ACF's 'image' field) or an attachment ID (from WordPress featured image).
    if (is_numeric($field_value) && wp_attachment_is_image($field_value)) {
        $image_id = $field_value;
    } elseif (is_array($field_value) && isset($field_value['ID']) && wp_attachment_is_image($field_value['ID'])) {
        // If it's a featured image from WordPress, get the attachment ID.
        $image_id = $field_value['ID'];
    } else {
        // If the field value is neither an image ID nor a valid attachment, return an empty array.
        return array();
    }

    // Get the full-size image URL, height, and width.
    $full_size = wp_get_attachment_image_src($image_id, $size);
    $image_url = $full_size[0];
    $image_width = $full_size[1];
    $image_height = $full_size[2];

    // Generate the 'srcset' attribute using wp_get_attachment_image_srcset().
    $srcset = wp_get_attachment_image_srcset($image_id, $size);

    // Get the alt text for the image.
    $alt_text = get_post_meta($image_id, '_wp_attachment_image_alt', true);

    // Return the image data as an array.
    return array(
        'url' => $image_url,
        'width' => $image_width,
        'height' => $image_height,
        'alt' => $alt_text,
        'srcset' => $srcset,
    );
}



function service_add_acf_columns ( $columns ) {
    $new_columns = array (
      'cb' => '<input type="checkbox" />',
      'featured_image' => __( 'Icon' ),
      'title' => __('Title'),
      'description' => __ ( 'Description' ),
      'date' => __('Date'),
    );
    unset($columns['cb']);
    unset($columns['title']);
    unset($columns['date']);
    return $new_columns + $columns; // This way your custom columns are at the end
 }
 add_filter ( 'manage_service_posts_columns', 'service_add_acf_columns' );

function service_custom_column ( $column, $post_id ) {
   switch ( $column ) {
    case 'featured_image':
        //$thumbnail = get_the_post_thumbnail( $post_id, array( 50, 50 ) );
        $the_image_id = get_field('icon', $post_id);
        if ($the_image_id) {
            $image_url = wp_get_attachment_image_src( $the_image_id, array(50,50) )[0];
            echo '<img src="'.$image_url.'" width="50" height="50" />';
        } else {
            echo 'N/A';
        }
        break;
     case 'description':
        $content = get_post_meta ( $post_id, 'excerpt', true );
        $content = wp_strip_all_tags($content);
        $words = preg_split('/\s+/', $content);
        $excerpt = implode(' ', array_slice($words, 0, 10));
        echo $excerpt;

        if (count($words) > 10) {
            echo ' [...]';
        }
       break;
   }
}
add_action ( 'manage_service_posts_custom_column', 'service_custom_column', 10, 2 );


// Custom columns for 'team' CPT
function team_add_acf_columns( $columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'featured_image' => __( 'Image' ),
        'title' => __( 'Title' ),
        'role' => __( 'Role' ),
        //'credentials' => __( 'Credentials' ),
        'description' => __( 'Description' ),
        'date' => __( 'Date' ),
    );
    unset( $columns['cb'] );
    unset( $columns['title'] );
    unset( $columns['date'] );
    return $new_columns + $columns; // This way your custom columns are at the end
}
add_filter( 'manage_team_posts_columns', 'team_add_acf_columns' );

// Populate custom columns for 'team' CPT
function team_custom_column( $column, $post_id ) {
    switch ( $column ) {
        case 'featured_image':
            $thumbnail = get_the_post_thumbnail( $post_id, array( 50, 50 ) );
            if ( $thumbnail ) {
                echo $thumbnail;
            } else {
                echo 'N/A';
            }
            break;
        case 'role':
            $role = get_field( 'designation', $post_id ); // Assuming the ACF field name is 'services'
            echo $role;
            break;
        case 'credentials':
            $credentials = get_field( 'role', $post_id ); // Assuming the ACF field name is 'services'
            echo $credentials;
            break;
        case 'description':
            $content = get_the_content();
            $content = wp_strip_all_tags($content);
            $words = preg_split('/\s+/', $content);
            $excerpt = implode(' ', array_slice($words, 0, 10));
            echo $excerpt;

            if (count($words) > 10) {
                echo ' [...]';
            }
            break;
    }
}
add_action( 'manage_team_posts_custom_column', 'team_custom_column', 10, 2 );







function client_add_acf_columns ( $columns ) {
    $new_columns = array (
      'cb' => '<input type="checkbox" />',
      'title' => __('Name'),
      'testimonial' => __ ( 'Testimonial' ),
      'category' => __ ( 'Category' ),
      'date' => __('Date'),
    );
    unset($columns['cb']);
    unset($columns['title']);
    unset($columns['date']);
    return $new_columns + $columns; // This way your custom columns are at the end
 }
 add_filter ( 'manage_client_posts_columns', 'client_add_acf_columns' );

function client_custom_column ( $column, $post_id ) {
   switch ( $column ) {
        case 'testimonial':
            $content = get_the_content();
            $content = wp_strip_all_tags($content);
            $words = preg_split('/\s+/', $content);
            $excerpt = implode(' ', array_slice($words, 0, 10));
            echo $excerpt;

            if (count($words) > 10) {
                echo ' [...]';
            }
        break;
        case 'category':
            echo get_post_meta ( $post_id, 'category', true );
            break;
   }
}
add_action ( 'manage_client_posts_custom_column', 'client_custom_column', 10, 2 );





function add_custom_inline_admin_css() {
    // Check if the current screen corresponds to the 'case' custom post type
    $screen = get_current_screen();
    if (($screen && $screen->post_type === 'team') || ($screen && $screen->post_type === 'service')) {
        ?>
        <style>
            /* Customizing width of featured image column */
            #featured_image {
                width: 80px; /* Adjust the width as needed */
            }

            /* Adjusting thumbnail size within the column */
            #featured_image img {
                max-width: 50px;
                height: auto;
            }
        </style>
        <?php
    }
}
add_action('admin_head', 'add_custom_inline_admin_css');


// Move Yoast to bottom
function wpcover_move_yoast() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'wpcover_move_yoast');

function webp($image_url) {
    if (strpos($image_url, '.webp') === false) {
        $image_url = str_replace('.png', '.png.webp', $image_url);
        $image_url = str_replace('.jpg', '.jpg.webp', $image_url);
        $image_url = str_replace('.jpeg', '.jpeg.webp', $image_url);
    }
    return $image_url;
}

function convertYoutubeUrlToEmbedUrl($url) {
    $video_id = null;
    
    // Parse URL
    $parsed_url = parse_url($url);
    
    // Check if host contains 'youtube' or 'youtu.be'
    if (strpos($parsed_url['host'], 'youtube') !== false) {
        // Parse query string
        parse_str($parsed_url['query'], $query_array);
        
        // Get video ID from 'v' parameter
        if (isset($query_array['v']) && strlen($query_array['v']) > 0) {
            $video_id = $query_array['v'];
        }
    } elseif (strpos($parsed_url['host'], 'youtu.be') !== false) {
        // Get video ID from path
        $path_segments = explode('/', trim($parsed_url['path'], '/'));
        if (count($path_segments) > 0 && strlen($path_segments[0]) > 0) {
            $video_id = $path_segments[0];
        }
    }

    // Check if video ID is valid and construct embed URL
    if ($video_id !== null) {
        return 'https://www.youtube.com/embed/' . $video_id;
    } else {
        return null; // Or throw an exception, or return the original URL, depending on your use-case
    }
}
