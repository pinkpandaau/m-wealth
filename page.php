<?php if ( !empty( get_the_content() ) ){ ?>
	
	<div class="default_page_wrapper container">

	    <div class="row">

	    	<div class="col-lg-10 offset-lg-1">
	    	
	    	    <?php while (have_posts()) : the_post(); ?>
					<?php //get_template_part('templates/page', 'header'); ?>
					<?php get_template_part('templates/content', 'page'); ?>
				<?php endwhile; ?>
	    	
	    	</div><!-- end col-lg-10 offset-lg-1 -->
		   
		</div><!-- end container -->

	</div><!-- end default_page_wrapper row -->

<?php } ?>

<?php get_template_part('templates/flexible-content'); ?>